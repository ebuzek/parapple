import Vue from 'vue'
import Router from 'vue-router'
import Home from 'components/Home'
import Docs from 'components/docs/Main'


import Compiler from 'parascript/compiler.js'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/docs',
      name: 'Docs',
      component: Docs
    }
  ]
})
