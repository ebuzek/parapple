class Global {

    constructor() {
        if (!window.PARAPPLE) {
            window.PARAPPLE = {};
        }
        this.PARAPPLE = window.PARAPPLE;
    }

    /**
     * Make object accessible in global namespace.
     * given object can be accessed anywhere under window.PARAPPLE.key
     * 
     * @param object 
     * @param key 
     */
    publish(object, key) {
        this.PARAPPLE[key] = object;
    }

}

// Export singleton instance of Global
let global = new Global();
export default global;