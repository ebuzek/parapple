export default class Stack {

    constructor() {
        this.array = [];
    }

    push(e) {
        this.array.push(e);
    }

    pop() {
        return this.array.pop();
    }

    // Pop N elements and return them as array
    popN(n) {
        return this.array.splice(this.array.length - n, n);
    }

    peek() {
        return this.array[this.array.length - 1];
    }

    peekBottom() {
        return this.array[0];
    }

    size() {
        return this.array.length;
    }

    empty() {
        return this.array.length == 0;
    }

}