// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import global from 'utils/global.js'

import * as ast from 'parascript/ast.js'
global.publish(ast, "ast")

require('utils/feedback.js')

// Make jQuery global
window.$ = require('jquery');

// vue-strap
import { alert, modal, radio, tooltip, tab, tabs, select, option, checkbox } from 'vue-strap'
import iconBtn from 'components/utils/vuestrap/IconBtn';
Vue.component('alert', alert)
Vue.component('modal', modal)
Vue.component('radio', radio)
Vue.component('tab', tab)
Vue.component('tabs', tabs)
Vue.component('tooltip', tooltip)
Vue.component('checkbox', checkbox)
Vue.component('iconBtn', iconBtn)
Vue.component('vSelect', select)
Vue.component('vOption', option)

// FontAwesome icons
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon.vue'
Vue.component('icon', Icon)

// Keyboard shortcuts
Vue.use(require('vue-shortkey'))

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})