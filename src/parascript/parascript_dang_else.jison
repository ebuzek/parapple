/* PARASCRIPT JISON LEXER & PARSER */

// Operator precedence follows https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Operators/Operator_Precedence

%lex

// FLEX DEFINITIONS

IdentifierStart [$_a-zA-Z]
IdentifierPart {IdentifierStart}|[0-9]
Identifier {IdentifierStart}{IdentifierPart}*

// should comply with http://www.ecma-international.org/ecma-262/5.1/Ecma-262.pdf page 20
NonZeroDigit [1-9]
DecimalDigit [0-9]
DecimalIntegerLiteral '0'|{NonZeroDigit}[0-9]*
DecimalPart '.'{DecimalDigit}+
DecimalLiteral ({DecimalIntegerLiteral}?{DecimalPart})|({DecimalIntegerLiteral}{DecimalPart}?)
ExponentPart [Ee][+-]?{DecimalDigit}+
NumericLiteral {DecimalLiteral}{ExponentPart}?

DoubleStringCharacter ([^\"\\\n\r]+)
SingleStringCharacter ([^\'\\\n\r]+)
String (\"{DoubleStringCharacter}*\")|(\'{SingleStringCharacter}*\')

%%

// FLEX RULES

\s+                                %{ // ignore whitespace %}

// Comments
"/*"(.|\r|\n)*?"*/"                %{ // multi-line comment %}
"//".*($|\r\n|\r|\n)               %{ // single-line comment %}

// Reserved keywords
"if"                               return "IF";
"else"                             return "ELSE";
"var"                              return "VAR";
"for"                              return "FOR";
"while"                            return "WHILE";
"function"                         return "FUNCTION";
"return"                           return "RETURN";
"break"                            return "BREAK";
"continue"                         return "CONTINUE";

// Literals
"true"                             return "BOOLEAN";
"false"                            return "BOOLEAN";
{Identifier}                       return "IDENTIFIER";
{String}                           return "STRING";
{NumericLiteral}                   return "NUMERIC_LITERAL";

// Delimiters
"{"                                return "{";
"}"                                return "}";
"("                                return "(";
")"                                return ")";
"["                                return "[";
"]"                                return "]";
";"                                return ";";
":"                                return ":";
","                                return ",";
"."                                return ".";
"?"                                return "?";

// Post/prefix operators
"++"                               return "++";
"--"                               return "--";

// Arithmetic operators
"+"                                return "+";
"-"                                return "-";
"*"                                return "*";
"/"                                return "/";
"%"                                return "%";

// Logical operators
"||"                               return "||"
"&&"                               return "&&"

// Relational operators
"=="                               return "LOGICAL_EQUALITY";
"!="                               return "LOGICAL_EQUALITY";

">="                               return "RELATIONAL_OPERATOR";
"<="                               return "RELATIONAL_OPERATOR";
">"                                return "RELATIONAL_OPERATOR";
"<"                                return "RELATIONAL_OPERATOR";

// Assignment operators
"+="                               return "ASSIGNMENT_OPERATOR";
"-="                               return "ASSIGNMENT_OPERATOR";
"*="                               return "ASSIGNMENT_OPERATOR";
"/="                               return "ASSIGNMENT_OPERATOR";
"="                                return "=";

// Unary operators
"!"                                return "!"

// End of file
<<EOF>>                            return 'EOF'
// Anything else is an invalid character
.                                  return 'Invalid_character'

%%

/lex

// BISON GRAMMAR

// Start symbol
%start Program

%left "ELSE"

%%

Program
    : ProgramElements EOF
        {
            $$ = new ListASTNode(new SourceLocation(@1, @2), $1);
            return $$;
        }
    ;

ProgramElements
    : ProgramElements ProgramElement
        {
            $$ = $1.concat($2);
        }
    |
        {
            $$ = [];
        }
    ;

ProgramElement
    : Statement
    | FunctionDeclaration
    ;


FunctionDeclaration
    : "FUNCTION" "IDENTIFIER" "(" ArgumentList ")" Block
      {
          $$ = new FunctionASTNode(new SourceLocation(@1, @6), $2, $4, $6);
      }
    ;

ArgumentList
    : // Empty argument list allowed
    | ArgumentList "," Argument
    | Argument
    ;

Argument
    : "IDENTIFIER"
    ;

Statement
    : ConditionalStatementWithoutElse
    | X
    ;

StatementWithElse
    : ConditionalStatementWithElse
    | X
    ;

X
:Block
| ExpressionStatement
| BreakStatement
| ContinueStatement
| ReturnStatement
;

ConditionalStatementWithoutElse
      : "IF" "(" Expression ")" Statement
        {
          $$ = new IfASTNode(new SourceLocation(@1, @5), $3, $5);
        }
        | "IF" "(" Expression ")" StatementWithElse "ELSE" Statement
          {
            $$ = new IfASTNode(new SourceLocation(@1, @7), $3, $5, $7);
          }
      ;

ConditionalStatementWithElse
      : "IF" "(" Expression ")" StatementWithElse "ELSE" StatementWithElse
        {
          $$ = new IfASTNode(new SourceLocation(@1, @7), $3, $5, $7);
        }
      ;

Block
      : "{" StatementList "}"
          {
            $$ = new ListASTNode(new SourceLocation(@1, @3), $2);
          }
      ;

BreakStatement
      : "BREAK" ";"
          {
            $$ = new BreakASTNode(new SourceLocation(@1, @2));
          }
      ;

ContinueStatement
      : "CONTINUE" ";"
          {
            $$ = new ContinueASTNode(new SourceLocation(@1, @2));
          }
      ;

ReturnStatement
      : "RETURN" ";"
          {
              $$ = new ReturnASTNode(new SourceLocation(@1, @2), null);
          }
      | "RETURN" Expression ";"
          {
              $$ = new ReturnASTNode(new SourceLocation(@1, @2), $2);
          }
      ;

IterationStatement
    : ForStatement
    | WhileStatement
    ;

ForStatement
    : "FOR" "(" ExpressionStatement Expression ";" Expression ")" Statement
      {
        $$ = new ForASTNode(new SourceLocation(@1, @8),  $3, $4, $6, $8);
      }
    ;

WhileStatement
    : "WHILE" "(" Expression ")" Statement
        {
          $$ = new WhileASTNode(new SourceLocation(@1, @5), $3, $5);
        }
    ;

ExpressionStatement
    : ";"
      {
        $$ = new EmptyASTNode();
      }
    | VarStatement
    | ExpressionNoObjectLiteral ";"
    ;

VarStatement
    : "VAR" DeclarationList ";"
        {
          $$ = new ListASTNode(new SourceLocation(@1, @3), $2);
        }
    ;

DeclarationList
    : Declaration
      {
        $$ = [ $1 ];
      }
    | DeclarationList "," Declaration
      {
        $$ = $1.concat($3)
      }
    ;

Declaration
    : "IDENTIFIER"
        {
          $$ = new VarASTNode(new SourceLocation(@1, @1), $1, null);
        }
    | "IDENTIFIER" "=" Expression
        {
          $$ = new VarASTNode(new SourceLocation(@1, @3), $1, $3);
        }
    ;

StatementList
     : StatementList Statement
          {
              $$ = $1.concat($2);
          }
     |
          {
              $$ = [];
          }
      ;

ExpressionList
    :
    | ExpressionList "," Expression
        {
          $$ = $1.concat($3);
        }
    | Expression
        {
          $$ = [ $1 ];
        }
    ;

/*
 * Expressions
 */
Expression
    : ExpressionNoObjectLiteral
    | ObjectLiteral
    ;

ExpressionNoObjectLiteral
    : AssignmentExpression
    ;

AssignmentExpression
    : TernaryExpression
    | "IDENTIFIER" "=" Expression
      {
          $$ = new IdentifierAssignmentASTNode(new SourceLocation(@1, @3), $1, $3);
      }
    | MemberExpression "=" Expression
      {
          $$ = new MemberAssignmentASTNode(new SourceLocation(@1, @3), $1, $3);
      }
    ;

TernaryExpression
    : LogicalOrExpression
    | LogicalOrExpression "?" Expression ":" TernaryExpression
      {
        $$ = new TernaryExpressionASTNode(new SourceLocation(@1, @5), $1, $3, $5);
      }
    ;

LogicalOrExpression
    : LogicalAndExpression
    | LogicalOrExpression "||" LogicalAndExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

LogicalAndExpression
    : EqualityExpression
    | LogicalAndExpression "&&" EqualityExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

EqualityExpression
    : RelationalExpression
    | EqualityExpression "LOGICAL_EQUALITY" RelationalExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

RelationalExpression
    : AddSubExpression
    | RelationalExpression "RELATIONAL_OPERATOR" AddSubExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

AddSubExpression
    : MulDivExpression
    | AddSubExpression AddSubOperator MulDivExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

AddSubOperator
    : "+"
    | "-"
    ;

MulDivExpression
    : UnaryExpression
    | MulDivExpression MulDivOperator UnaryExpression
      {
        $$ = new BinaryExpressionASTNode(new SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

MulDivOperator
    : "*"
    | "\"
    | "%"
    ;

UnaryExpression
    : PostfixExpression
    | PrefixExpression
    | MemberExpression
    | PrimaryExpression
    | UnaryOperator UnaryExpression
      {
        $$ = new UnaryExpressionASTNode(new SourceLocation(@1, @2), $1, $2);
      }
    ;

UnaryOperator
    : "-"
    | "+"
    | "!"
    ;

PostfixExpression
    : "IDENTIFIER" PostfixOperator
      {
        $$ = new PostfixExpressionASTNode(new SourceLocation(@1, @2), $1, $2);
      }
    ;

PostfixOperator
    : "++"
    | "--"
    ;

PrefixExpression
    : PrefixOperator "IDENTIFIER"
      {
        $$ = new PrefixExpressionASTNode(new SourceLocation(@1, @2), $2, $1);
      }
    ;

PrefixOperator
    : "++"
    | "--"
    ;

MemberExpression
    : PrimaryExpression "[" Expression "]"
      {
        $$ = new MemberExpressionASTNode(new SourceLocation(@1, @4), $1, $3, true);
      }
    | MemberExpression "[" Expression "]"
      {
        $$ = new MemberExpressionASTNode(new SourceLocation(@1, @4), $1, $3, true);
      }
    | PrimaryExpression "." "IDENTIFIER"
      {
        $$ = new MemberExpressionASTNode(new SourceLocation(@1, @3), $1, $3);
      }
    | MemberExpression "." "IDENTIFIER"
      {
        $$ = new MemberExpressionASTNode(new SourceLocation(@1, @3), $1, $3);
      }
    ;

PrimaryExpression
    : Literal
    | Identifier
    | FunctionCall
    | "(" Expression ")"
      {
        $$ = $2
      }
    ;

FunctionCall
    : "IDENTIFIER" "(" ExpressionList ")"
      {
        $$ = new FunctionCallASTNode(new SourceLocation(@1, @4), $1, $3);
      }
    ;

Identifier
  : "IDENTIFIER"
    {
      $$ = new LiteralASTNode(new SourceLocation(@1, @1), $1);
    }
  ;

Literal
    : BooleanLiteral
    | NumericLiteral
    | StringLiteral
    | ArrayLiteral
    | NullLiteral
    ;

BooleanLiteral
  : "BOOLEAN"
      {
        $$ = new LiteralASTNode(new SourceLocation(@1, @1), $1);
      }
  ;

NumericLiteral
  : "NUMERIC_LITERAL"
    {
      $$ = new LiteralASTNode(new SourceLocation(@1, @1), $1);
    }
  ;

StringLiteral
  : "STRING"
      {
        $$ = new LiteralASTNode(new SourceLocation(@1, @1), $1);
      }
  ;

ArrayLiteral
  : "[" + ExpressionList + "]"
    {
      $$ = new ArrayLiteralASTNode(new SourceLocation(@1, @3), $2)
    }
  ;

ObjectLiteral
  : "{" + PropertyList + "}"
    {
      $$ = new ObjectLiteralASTNode(new SourceLocation(@1, @3), $2)
    }
  ;

PropertyList
  :
  | PropertyList "," Property
    {
      $$ = $1.concat($3);
    }
  | Property
    {
      $$ = [ $1 ];
    }
  ;

Property
  : "IDENTIFIER" ":" Expression
    {
      $$ = new PropertyASTNode(new SourceLocation(@1, @3), $1, $3);
    }
  ;

NullLiteral
  : "NULL"
      {
        $$ = new LiteralASTNode(new SourceLocation(@1, @1), null);
      }
  ;
*/

%%
