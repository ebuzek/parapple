import { CodeError } from 'common/errors.js'
import { FunctionCallInstruction, PackageFunctionCallInstruction } from 'runtime/instructions.js';
/**
 * Support class for tracking declared identifiers
 */
export default class IdentifierTable {

  constructor(packageFunctions, packageConstants) {
    // User functions
    // Map functionName -> { address, argCount }
    this.functions = {};

    // Package functions and constants
    // Map packageFunctionName -> function
    this.packageFunctions = packageFunctions;
    // Map packageConstantName -> value
    this.packageConstants = packageConstants;

    // Identifiers scope
    this.currentScope = {
        // Map variableName -> true
        identifiers: {},
        // This scope is the global scope
        parentScope: null
    };
    this.globalScope = this.currentScope;
  }

  /*
   * Functions
   */

  /**
   * Add function call identifier to the lookup table.
   * All function call identifiers are added before the actual code generation.
   * This implements function hoisting and allows forward function calls.
   */
  declareFunction(name, location, argCount) {
    this.checkIdentifierAvailable(name, location);
    this.functions[name] = {
      address: -1, // location not known at this point
      argCount: argCount
    }
  }

  /**
   * Update the actual instruction index of the begging of this function code.
   * This is only known during the code generation phase.
   */
  setFunctionAddress(name, address, location) {
    if (!this.isFunctionDeclared(name)) {
      // Undeclared function
      throw new CodeError("Cannot set function address for undeclared function '" + name + "'", location);
    }
    this.functions[name].address = address;
  }

  // Return instruction with the proper jump to function address / native function
  createFunctionCallInstruction(name, location, argCount) {
    if (this.isFunctionDeclared(name)) {
      // User function
      var expectedArgCount = this.functions[name].argCount;
      // Verify arg count
      this.checkArgCount(expectedArgCount, argCount, name, location);
      return new FunctionCallInstruction(location, name, argCount);  
    } else if (this.isPackageFunctionDeclared(name)) {
      // Package function
      var _f = this.packageFunctions[name];
      // Verify arg count
      this.checkArgCount(_f.length - 1, argCount, name, location); // Note: first argument is agent
      return new PackageFunctionCallInstruction(location, name, argCount, _f);
    }
    // Undeclared function
    throw new CodeError("Undeclared function '" + name + "'", location);
    
  }

  // Helper function that verifies that provided number of arguments to a function is correct
  checkArgCount(expectedArgCount, argCount, name, location) {
    if (argCount > expectedArgCount) {
        throw new CodeError("Too many arguments passed to function '" + name + "' (expected " + expectedArgCount + ")", location);
    }
    if (argCount < expectedArgCount) {
       throw new CodeError("Too few arguments passed to function '" + name + "' (expected " + expectedArgCount + ")", location);
    }
  }

  // set the proper address to the FunctionCallInstruction
  fixFunctionCallInstruction(instruction) {
    var address = this.functions[instruction.params.name].address;
    if (address < 0) {
      throw new CodeError("Cannot fix function call instruction, function address unknown: '" + instruction.params.name + "'", instruction.location);
    }
    instruction.setAddress(address);
  }

  isFunctionDeclared(name) {
    return this.functions.hasOwnProperty(name);
  }

  isPackageFunctionDeclared(name) {
    return this.packageFunctions.hasOwnProperty(name);
  }

  /*
   * Package constants
   */
  isPackageConstantDeclared(name) {
    return this.packageConstants.hasOwnProperty(name);
  }

  getPackageConstant(name) {
    return this.packageConstants[name];
  }

  /*
   * Identifiers
   */

  declareIdentifier(name, location) {
    this.checkIdentifierAvailable(name, location);
    // Add identifier to current scope
    this.currentScope.identifiers[name] = true;
  }

  // return true if identifier declared in current or any parent scope
  isIdentifierDeclared(name) {
    var scope = this.currentScope; // Start with bottom-most scope
    while (scope) {
      if (scope.identifiers.hasOwnProperty(name)) {
        return true; // identifier found
      }
      // identifier not found in this scope, go level up
      scope = scope.parentScope;
    }
    return false; // not found
  }

  // throw error if variable or package constant undeclared
  checkIdentifierOrConstantDeclared(name, location) {
    if (!this.isIdentifierDeclared(name) && !this.isPackageConstantDeclared(name)) {
      throw new CodeError("Undeclared identifier '" + name + "'", location);
    }
    // ok
  }

  // throw error if variable undeclared or declared as constant
  checkIdentifierAssignable(name, location) {
    this.checkIdentifierOrConstantDeclared(name, location);
    if (this.isPackageConstantDeclared(name)) {
      throw new CodeError("Cannot assign package constant '" + name + "'", location);
    }
    // ok
  }

  /*
   * Common
   */
  // throw error if identifier name already used (by function/constant/var)
  checkIdentifierAvailable(name, location) {
    if (this.isIdentifierDeclared(name)) {
      // Error: redeclared identifier
      throw new CodeError("Redeclared identifier '" + name + "'", location);
    }
    if (this.isFunctionDeclared(name)) {
      // Identifier name taken by function
      throw new CodeError("Identifier '" + name + "' is already declared as a function", location);
    }
    if (this.isPackageFunctionDeclared(name)) {
      // Identifier name taken by package function
      throw new CodeError("Identifier '" + name + "' is already used by a package function", location);
    }
    if (this.isPackageConstantDeclared(name)) {
      // Identifier name taken by package constant
      throw new CodeError("Identifier '" + name + "' is already used by a package constant", location);
    }
  }

  /*
   * Scope handling
   */

  pushScope() {
    var newScope = {
      identifiers: {},
      parentScope: this.currentScope
    };
    this.currentScope = newScope;
  }

  popScope() {
    // throw away current scope
    this.currentScope = this.currentScope.parentScope;
  }

  // Check global scope
  // Return true if provided identifier is declared on global scope;
  // If no identifier is specified, return true if the current scope is global.
  isGlobal(identifier) {
    if (!identifier) {
      // No identifier specified; return true if current scope is global
      return this.currentScope.parentScope == null;
    } else {
      // Return true if identifier declared on global
      return this.globalScope.identifiers.hasOwnProperty(identifier);
    }
  }

}
