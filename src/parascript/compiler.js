import { CodeError } from 'common/errors.js'
import { SourceLocation } from 'parascript/ast.js'
import parascript from 'parascript/parser/parascript.js'

/**
 * Compiler
 */
export default class Compiler {

  constructor() {
  }

  compile(source, agentType) {
    // Parse source and build parse tree
    var parseTree;
    try {
      parseTree  = parascript.parse(source);
    } catch(e) {
      // Parser error; throw CodeError
      var location = null;
      if (e.hash) {
        location = new SourceLocation(e.hash.loc);
      }
      throw new CodeError(e.message ? e.message : "Parse error", location);
    }

    if (!parseTree) {
      // Unknown error; Parser did not throw error, but parse tree was not built
      throw new CodeError("Unknown parse error, cannot build parse tree.");
    }

    // Generate instructions from parse tree and agent API
    return parseTree.generate(agentType.api, agentType.costs);
  }

  

}
