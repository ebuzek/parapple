import IdentifierTable from 'parascript/identifierTable.js';
import * as instructions from 'runtime/instructions.js';
import { CodeError } from 'common/errors.js'

export class SourceLocation {

  // lastToken is optional
  constructor(firstToken, lastToken) {
    this.startLine = firstToken.first_line;
    this.startColumn = firstToken.first_column;
    if (lastToken != null) {
        this.endLine = lastToken.last_line;
        this.endColumn = lastToken.last_column;
    } else {
        this.endLine = firstToken.last_line;
        this.endColumn = firstToken.last_column;
    }
  }

}

export class ASTNode {

  constructor(location, children) {
    this.location = location;

    // Copy children array. Remove null children.
    this.children = [];
    if (children) {
      for (var child of children) {
        if (child) {
          child.parent = this;
          this.children.push(child);
        }
      }
    }
  }

  /**
   * Visit all children of a given node
   *
   * This allows to walk an entire (sub)tree
   * and perform any given operation on each node.
   *
   * Nodes are visited depth-first.
   *
   * @param operation Function which takes node as argument.
   */
  visit(operation) {
    for (var child of this.children) {
      child.visit(operation);
    }
    if (operation) {
      operation(this);
    }
  }

  /**
   * Generate list of executable instructions.
   * 
   * @param agentAPI pre-processed packageFunctions and packageConstants from the APIs of the agent type
   */
  generate(agentAPI, agentCosts) {

    // Instantiate IdentifierTable for declaration tracking
    var identifierTable = new IdentifierTable(agentAPI.packageFunctions, agentAPI.packageConstants);

    // Inject identifierTable to all nodes
    this.visit(function(node) {
      node.identifierTable = identifierTable;
    });

    // Function hoisting: Declare all functions; add their names to the lookup table
    for (const node of this.children) {
      if (node.declare) { // test if declare is implemented; definitely applies for FunctionASTNode
          node.declare();
      }
    }

    // Generate instructions for all nodes to the instruction list
    var instructionList = [];
    this.append(instructionList);

    // Program termination - HALT.
    instructionList.push(new instructions.HaltInstruction());

    // Post-processing: fix addresses and set costs functions
    let address = 0;
    for (const instruction of instructionList) {
      // Fix function call instructions: update actual addresses
      if (instruction.id == instructions.INSTRUCTION_TYPES.CALL) {
        this.identifierTable.fixFunctionCallInstruction(instruction);
      }
      // Set proper instruction cost functions
      instruction.cost = agentCosts[instruction.id]; 
      // Set absolute address to each instruction
      instruction.address = address++;
    }

    return instructionList;
  }

  append(instructionList) {
  }

}

export class ListASTNode extends ASTNode {

  /**
   * @param nestedScope boolean true if nested scope should be pushed
   */
  constructor(location, children, nestedScope) {
    super(location, children);
    this.nestedScope = nestedScope;
  }

  append(instructionList) {
    if (this.nestedScope) {
      // Enter nested scope
      this.identifierTable.pushScope();
    }

    for (const node of this.children) {
      // Generate instruction for all nodes recursively
      node.append(instructionList);
    }

    if (this.nestedScope) {
      // Exit nested scope
      this.identifierTable.popScope();
    }
  }

}

export class EmptyASTNode extends ASTNode {

  constructor() {
    super(null);
  }

  append(instructionList) {
  }

}

export class IfASTNode extends ASTNode {

  constructor(location, condition, body, elsebody) {
    super(location, [condition, body, elsebody]);
    this.condition = condition;
    this.body = body;
    this.elsebody = elsebody;
  }

  append(instructionList) {
    this.condition.append(instructionList);
    var condJmp = new instructions.ConditionalJumpInstruction(this.location);
    instructionList.push(condJmp);
    this.body.append(instructionList);

    if (this.elsebody) {
      // jump behind else block
      var elseJmp = new instructions.JumpInstruction(this.location);
      elseJmp.isElse = true; // Lockstep scheduler helper flag
      instructionList.push(elseJmp);
    }

    condJmp.setAddress(instructionList.length);

    if (this.elsebody) {
      this.elsebody.append(instructionList);
      elseJmp.setAddress(instructionList.length);
    }
  }

}

export class ReturnASTNode extends ASTNode {

  constructor(location, expression) {
    super(location, [expression]);
    this.expression = expression;
  }

  // return node may be nested only under FunctionASTNode
  hasEnclosingFunction() {
    var n = this;
    while(n.parent) {
      if (n.isFunctionDeclaration) {
        return true; // enclosing function node found
      }
      n = n.parent; // step up in the tree
    }
    // no enclosing function node found
    return false;
  }

  append(instructionList) {
    if (!this.hasEnclosingFunction()) {
      // Error: return node outside of function declaration
      throw new CodeError("Return statement can be used only in a function.", this.location);
    }
    if (this.expression) {
      this.expression.append(instructionList);
    }
    instructionList.push(new instructions.ReturnInstruction(this.location));
  }

}

export class ExpressionStatementASTNode extends ASTNode {

  constructor(location, expression) {
    super(location, [expression]);
    this.expression = expression;
  }

  append(instructionList) {
    if (this.expression) {
      this.expression.append(instructionList);
    }
    // Expression statement has to be followed by a POP instructrion;
    // otherwise, there would be an extra value on top of the stack.
    instructionList.push(new instructions.PopInstruction(this.location));
  }

}

export class LiteralASTNode extends ASTNode {

  constructor(location, value) {
    super(location);
    // Value must be evaluated because it comes as string
    this.value = eval(value);
  }

  append(instructionList) {
    instructionList.push(new instructions.PushLiteralInstruction(this.location, this.value));
  }

}

export class IdentifierASTNode extends ASTNode {

  constructor(location, identifier) {
    super(location);
    this.identifier = identifier;
  }

  append(instructionList) {
    this.identifierTable.checkIdentifierOrConstantDeclared(this.identifier, this.location);
    if (this.identifierTable.isPackageConstantDeclared(this.identifier)) {
      // Package constant
      instructionList.push(new instructions.PushLiteralInstruction(this.location, this.identifierTable.getPackageConstant(this.identifier)));
    } else {
      // Variable
      instructionList.push(new instructions.PushInstruction(this.location, this.identifier, this.identifierTable.isGlobal(this.identifier)));  
    }
  }

}

export class ArrayLiteralASTNode extends ASTNode {

  constructor(location, expressionList) {
    super(location, expressionList);
    this.expressionList = expressionList;
  }

  append(instructionList) {
    var length = 0;
    if (this.expressionList) {
      for (var expression of this.expressionList) {
        if (expression) {
          expression.append(instructionList);
          length++;
        }
      }
    }
    instructionList.push(new instructions.ArrayInstruction(this.location, length));
  }

}

export class MapLiteralASTNode extends ASTNode {

  constructor(location, propertyList) {
    super(location, propertyList);
    this.propertyList = propertyList;
  }

  append(instructionList) {
    var propertyNames = [];
    if (this.propertyList) {
      for (var property of this.propertyList) {
        if (property) {
          property.append(instructionList);
          propertyNames.push(property.property);
        }
      }
    }
    instructionList.push(new instructions.MapInstruction(this.location, propertyNames));
  }

}

export class PropertyASTNode extends ASTNode {

  constructor(location, property, expression) {
    super(location, [expression]);
    this.property = property;
    this.expression = expression;
  }

  append(instructionList) {
    this.expression.append(instructionList);
  }

}

export class BinaryExpressionASTNode extends ASTNode {

  constructor(location, left, right, operator) {
    super(location, [left, right]);
    this.left = left;
    this.right = right;
    this.operator = operator;
  }

  append(instructionList) {
    this.left.append(instructionList);
    // Short-circuit?
    let shortCircuitJump;
    if (this.operator == "&&") {
      // Jump if left-hand side evaluated to a falsy value
      shortCircuitJump = new instructions.ShortCircuitConditionalJumpInstruction(this.location, false);
      instructionList.push(shortCircuitJump);
    } else if (this.operator == "||") {
      // Jump if left-hand side evaluated to a truthy value
      shortCircuitJump = new instructions.ShortCircuitConditionalJumpInstruction(this.location, true);
      instructionList.push(shortCircuitJump);
    }
    this.right.append(instructionList);
    instructionList.push(new instructions.BinaryOperationInstruction(this.location, this.operator));
    if (shortCircuitJump) {
      // Possible short-circuiting; set jump address
      shortCircuitJump.setAddress(instructionList.length);
    }
  }

}

export class TernaryExpressionASTNode extends IfASTNode {

  constructor(location, condition, first, second) {
    super(location, condition, first, second);
  }

}

export class UnaryExpressionASTNode extends ASTNode {

  constructor(location, operator, expression) {
    super(location, [expression]);
    this.operator = operator;
    this.expression = expression;
  }

  append(instructionList) {
    this.expression.append(instructionList);
    instructionList.push(new instructions.UnaryOperationInstruction(this.location, this.operator));
  }

}

export class PostfixExpressionASTNode extends ASTNode {

  constructor(location, identifier, operator) {
    super(location);
    this.operator = operator;
    this.identifier = identifier;

  }

  append(instructionList) {
    this.identifierTable.checkIdentifierAssignable(this.identifier, this.location);
    var global = this.identifierTable.isGlobal(this.identifier);
    instructionList.push(new instructions.PushInstruction(this.location, this.identifier, global));
    instructionList.push(new instructions.PushInstruction(this.location, this.identifier, global));
    instructionList.push(new instructions.UnaryOperationInstruction(this.location, this.operator));
    instructionList.push(new instructions.AssignInstruction(this.location, this.identifier, global, false));
  }

}

export class PrefixExpressionASTNode extends ASTNode {

  constructor(location, identifier, operator) {
    super(location);
    this.operator = operator;
    this.identifier = identifier;
  }

  append(instructionList) {
    this.identifierTable.checkIdentifierAssignable(this.identifier, this.location);
    var global = this.identifierTable.isGlobal(this.identifier);
    instructionList.push(new instructions.PushInstruction(this.location, this.identifier, global));
    instructionList.push(new instructions.UnaryOperationInstruction(this.location, this.operator));
    instructionList.push(new instructions.AssignInstruction(this.location, this.identifier, global, false));
    instructionList.push(new instructions.PushInstruction(this.location, this.identifier, global));
  }

}

export class MemberExpressionASTNode extends ASTNode {

  constructor(location, owner, field, evaluate) {
    super(location, evaluate ? [owner, field] : [owner]);
    this.owner = owner; // The indexable: some subtree leading to an array or map on the stack
    this.field = field; // The field: either expression or identifier name
    this.evaluate = evaluate; // Indicates whether to evaluate field as expression
  }

  append(instructionList, assignment) {
    this.owner.append(instructionList);
    if (this.evaluate) {
      // first, evaluate expression to determine the member field
      this.field.append(instructionList);
    }
    // Passing null fieldName when accessing using [expression] notation
    let fieldName = this.evaluate ? null : this.field;
    // Assignment indicates whether is read-member instruction or assign-member instruction
    if (!assignment) {
      // READ: Place member value onto the stack
      instructionList.push(new instructions.MemberInstruction(this.location, fieldName));
    } else {
      // WRITE: Assign member to a value from the stack
      instructionList.push(new instructions.MemberAssignmentInstruction(this.location, fieldName));
    }
  }

}

export class AssignmentASTNode extends ASTNode {

  constructor(location, expression, operator, children) {
    super(location, children);
    this.expression = expression;
    this.operator = operator;
  }

  append(instructionList) {
    // Evaluate the right hand side expression
    this.expression.append(instructionList);
    // Evaluate arithmetic operation if needed
    var assignmentOperator = this.getArithmeticOperator(this.operator);
    if (assignmentOperator) {
      // Push left hand side (identifier or member expression)
      this.appendLeftHandSide(instructionList);
      // Arithmetic operation
      instructionList.push(new instructions.BinaryOperationInstruction(this.location, assignmentOperator));
    }
    // Assign expression to left hand side
    this.appendAssignmentInstruction(instructionList);
  }

  appendLeftHandSide(instructionList) {
  }

  appendAssignmentInstruction(instructionList) {
  }

  getArithmeticOperator(assignmentOperator) {
    if (assignmentOperator == "=") {
      return null; // no arithmetic instruction necessary
    }
    if (assignmentOperator == "+=") {
      return "+";
    } else if (assignmentOperator == "-=") {
      return "-";
    } if (assignmentOperator == "/=") {
      return "/";
    } if (assignmentOperator == "*=") {
      return "*";
    }
    throw new CodeError("Assignment operator not known: " + assignmentOperator, this.location);
  }

}

export class IdentifierAssignmentASTNode extends AssignmentASTNode {

  constructor(location, identifier, expression, operator) {
    super(location, expression, operator, [expression]);
    this.identifier = identifier;
  }

  appendLeftHandSide(instructionList) {
    instructionList.push(new instructions.PushInstruction(this.location, this.identifier, this.identifierTable.isGlobal(this.identifier)));
  }

  appendAssignmentInstruction(instructionList) {
    this.identifierTable.checkIdentifierAssignable(this.identifier, this.location);
    // peek=true; leave assigned value on the stack (Issue #47)
    instructionList.push(new instructions.AssignInstruction(this.location, this.identifier, this.identifierTable.isGlobal(this.identifier), true));
  }

}

export class MemberAssignmentASTNode extends AssignmentASTNode {

  constructor(location, member, expression, operator) {
    super(location, expression, operator, [expression, member]);
    this.member = member;
  }

  appendLeftHandSide(instructionList) {
    this.member.append(instructionList);
  }

  appendAssignmentInstruction(instructionList) {
    this.member.append(instructionList, true); // Assign: true
  }

}

export class VarASTNode extends ASTNode {

  constructor(location, identifier, expression) {
    super(location, [expression]);
    this.identifier = identifier;
    this.expression = expression;
  }

  append(instructionList) {
    this.identifierTable.declareIdentifier(this.identifier, this.location);
    instructionList.push(new instructions.DeclareInstruction(this.location, this.identifier, this.identifierTable.isGlobal()));
    if (this.expression) {
      this.expression.append(instructionList);
      instructionList.push(new instructions.AssignInstruction(this.location, this.identifier, this.identifierTable.isGlobal(), false));
    }
  }

}

export class IterationASTNode extends ASTNode {

  constructor(location, children) {
    super(location, children);
    // Flag used for going through iteration statements to fill in break and continue instruction
    this.iterationNode = true;
  }

  /**
   * Fix jump instruction addresses for break and continue statements in this subtree
   *
   * @param continueAddr Address of the start of the iteration body
   * @param breakAddr Address of the end of the iteration body
   */
  fixBreakContinue(continueAddr, breakAddr) {

    // recursive procedure to find break and continue statements in the subtree
    var _recursive = function(nodes) {
      for (var node of nodes) {
        if (node.isBreak) {
          // break;
          node.instruction.setAddress(breakAddr);
        } else if (node.isContinue) {
          // continue;
          node.instruction.setAddress(continueAddr);
        } else if (!node.iterationNode && node.children) {
          // Iteration nodes in subtrees have fixed up break statements already.
          // Dive further to non-iteration nodes with subtrees.
          _recursive(node.children);
        }
      }
    }
    // start searching in the body of this iteration statement
    _recursive([this.body]);

  }

}

export class BreakContinueASTNode extends ASTNode {

  constructor(location) {
    super(location, null);
  }

  append(instructionList) {
    if (!this.hasEnclosingLoop()) {
      // Error: break/continue without an enclosing loop
      throw new CodeError("Break/continue statement cannot be used without an enclosing loop.", this.location);
    }
    this.instruction = new instructions.JumpInstruction(this.location);
    instructionList.push(this.instruction);
  }

  hasEnclosingLoop() {
    var n = this;
    while(n.parent) {
      if (n.iterationNode) {
        return true; // enclosing iteration loop found
      }
      n = n.parent; // step up in the tree
    }
    // no enclosing loop found
    return false;
  }

}

export class BreakASTNode extends BreakContinueASTNode {

  constructor(location) {
    super(location);
    this.isBreak = true;
  }

  append(instructionList) {
    super.append(instructionList);
    this.instruction.isBreak = true; // Lockstep scheduler helper flag
  }

}

export class ContinueASTNode extends BreakContinueASTNode {

  constructor(location) {
    super(location);
    this.isContinue = true;
  }

  append(instructionList) {
    super.append(instructionList);
    this.instruction.isContinue = true; // Lockstep scheduler helper flag
  }

}

export class ForASTNode extends IterationASTNode {

  constructor(location, initialiser, condition, iteration, body) {
    super(location, [initialiser, condition, iteration, body]);
    this.initialiser = initialiser;
    this.condition = condition;
    this.iteration = iteration;
    this.body = body;
  }

  append(instructionList) {
    // Enter nested scope (initialiser should be in a new scope)
    this.identifierTable.pushScope();

    if (this.initialiser) {
      this.initialiser.append(instructionList);
    }
    var startAddr = instructionList.length;
    var condJmp;
    if (this.condition) {
      this.condition.append(instructionList);
      condJmp = new instructions.ConditionalJumpInstruction(this.location);
      instructionList.push(condJmp);
    }
    if (this.body) {
      this.body.append(instructionList);
    }
    // When continue, jump to the iteration block (to increment the iteration var, for example)
    var continueAddr = instructionList.length;
    if (this.iteration) {
      this.iteration.append(instructionList);
      // Iteration is an expression, which adds a value onto the stack.
      // This value must be popped off of the stack (just like in ExpressionStatementASTNode)
      instructionList.push(new instructions.PopInstruction(this.iteration.location));
    }
    instructionList.push(new instructions.JumpInstruction(this.location, startAddr))
    if (condJmp) {
      condJmp.setAddress(instructionList.length);
    }

    // Exist nested scope
    this.identifierTable.popScope();

    // Lock-step scheduler uses this to handle continue JUMP
    condJmp.continueAddr = continueAddr; 

    this.fixBreakContinue(continueAddr, instructionList.length);
  }

}

export class WhileASTNode extends IterationASTNode {

  constructor(location, condition, body) {
    super(location, [condition, body]);
    this.condition = condition;
    this.body = body;
  }

  append(instructionList) {
    var startAddr = instructionList.length;
    var condJmp;
    if (this.condition) {
      this.condition.append(instructionList);
      condJmp = new instructions.ConditionalJumpInstruction(this.location);
      instructionList.push(condJmp);
    }
    if (this.body) {
      this.body.append(instructionList);
    }
    instructionList.push(new instructions.JumpInstruction(this.location, startAddr));
    if (condJmp) {
      condJmp.setAddress(instructionList.length);
    }

    var continueAddr = instructionList.length;

    // Lock-step scheduler uses this to handle continue JUMP
    condJmp.continueAddr = continueAddr; 

    this.fixBreakContinue(startAddr, continueAddr);
  }

}

export class FunctionASTNode extends ASTNode {

  constructor(location, name, args, body) {
    super(location, [body]);
    this.name = name;
    this.args = args;
    this.argCount = args ? args.length : 0;
    this.body = body;
    this.isFunctionDeclaration = true;
  }

  declare() {
    // Declare this function identifier name in the lookup table

    this.identifierTable.declareFunction(this.name, this.location, this.argCount);
  }

  append(instructionList) {
    // Skip the function block: 
    // jump instruction to the end of the function (so that the function is not executed when not called...)
    var skipJmp = new instructions.JumpInstruction(this.location);
    instructionList.push(skipJmp);

    // Update the actual instruction address of this function in the lookup table
    this.identifierTable.setFunctionAddress(this.name, instructionList.length, this.location);

    // Enter nested scope
    this.identifierTable.pushScope();

    // Pop and assign individual function args
    if (this.args && this.args.length > 0) {
      for (var arg of this.args) {
        // parameter is a declared identifier
        this.identifierTable.declareIdentifier(arg, this.location);
        // assign identifier value
        instructionList.push(new instructions.AssignInstruction(this.location, arg, false, false));
      }
    }

    // Function body
    // Prevent pushing scope (again) in function body instruction list.
    // Note: scoping of the argument identifiers works fine even without the following line...
    // It's just unnecessary to push scope in the list node if we've done it here already
    this.body.nestedScope = false;
    this.body.append(instructionList);

    // Always return from function body.
    // XXX: potential optimalization: check return not already present
    instructionList.push(new instructions.ReturnInstruction(this.location));

    // Exit nested scope
    this.identifierTable.popScope();

    // Update address of skip jump instruction
    skipJmp.setAddress(instructionList.length);
  }

}

export class FunctionCallASTNode extends ASTNode {

  constructor(location, name, args) {
    super(location, args);
    this.name = name;
    this.args = args;
    this.isFunctionCall = true;
  }

  append(instructionList) {
    // Evaluate function arguments (push them onto the stack)
    var argCount = 0;
    if (this.args) {
      // Place arguments in reversed order so they align with parameters when popped off
      for (var arg of this.args.reverse()) {
        if (arg) {
          arg.append(instructionList);
          argCount++;
        }
      }
    }
    // Create instruction (CALL or CALL_PACKAGE)
    // Identifier table checks the function exists and checks the number of arguments
    // Created instruction differs for user (CALL) and package (CALL_PACKAGE) functions!
    var functionCall = this.identifierTable.createFunctionCallInstruction(this.name, this.location, argCount);
    instructionList.push(functionCall);
  }

}
