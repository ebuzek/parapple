/* PARASCRIPT JISON LEXER & PARSER */

// Operator precedence follows https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Operators/Operator_Precedence

%lex

// FLEX DEFINITIONS

IdentifierStart [$_a-zA-Z]
IdentifierPart {IdentifierStart}|[0-9]
Identifier {IdentifierStart}{IdentifierPart}*

// should comply with http://www.ecma-international.org/ecma-262/5.1/Ecma-262.pdf page 20
NonZeroDigit [1-9]
DecimalDigit [0-9]
DecimalIntegerLiteral '0'|{NonZeroDigit}[0-9]*
DecimalPart '.'{DecimalDigit}+
DecimalLiteral ({DecimalIntegerLiteral}?{DecimalPart})|({DecimalIntegerLiteral}{DecimalPart}?)
ExponentPart [Ee][+-]?{DecimalDigit}+
NumericLiteral {DecimalLiteral}{ExponentPart}?

DoubleStringCharacter ([^\"\\\n\r]+)
SingleStringCharacter ([^\'\\\n\r]+)
String (\"{DoubleStringCharacter}*\")|(\'{SingleStringCharacter}*\')

%%

// FLEX RULES

\s+                                %{ // ignore whitespace %}

// Comments
"/*"(.|\r|\n)*?"*/"                %{ // multi-line comment %}
"//".*($|\r\n|\r|\n)               %{ // single-line comment %}

// Reserved keywords
"if"                               return "IF";
"else"                             return "ELSE";
"var"                              return "VAR";
"for"                              return "FOR";
"while"                            return "WHILE";
"function"                         return "FUNCTION";
"return"                           return "RETURN";
"break"                            return "BREAK";
"continue"                         return "CONTINUE";

// Literals
"true"                             return "BOOLEAN";
"false"                            return "BOOLEAN";
"null"                             return "NULL";
{Identifier}                       return "IDENTIFIER";
{String}                           return "STRING";
{NumericLiteral}                   return "NUMERIC_LITERAL";

// Delimiters
"{"                                return "{";
"}"                                return "}";
"("                                return "(";
")"                                return ")";
"["                                return "[";
"]"                                return "]";
";"                                return ";";
":"                                return ":";
","                                return ",";
"."                                return ".";
"?"                                return "?";

// Relational operators
"==="                              return "LOGICAL_EQUALITY";
"=="                               return "LOGICAL_EQUALITY";
"!=="                              return "LOGICAL_EQUALITY";
"!="                               return "LOGICAL_EQUALITY";

">="                               return "RELATIONAL_OPERATOR";
"<="                               return "RELATIONAL_OPERATOR";
">"                                return "RELATIONAL_OPERATOR";
"<"                                return "RELATIONAL_OPERATOR";

// Assignment operators
"+="                               return "ARITHMETIC_ASSIGNMENT_OPERATOR";
"-="                               return "ARITHMETIC_ASSIGNMENT_OPERATOR";
"*="                               return "ARITHMETIC_ASSIGNMENT_OPERATOR";
"/="                               return "ARITHMETIC_ASSIGNMENT_OPERATOR";
"="                                return "=";

// Post/prefix operators
"++"                               return "++";
"--"                               return "--";

// Arithmetic operators
"+"                                return "+";
"-"                                return "-";
"*"                                return "*";
"/"                                return "/";
"%"                                return "%";

// Logical operators
"||"                               return "||"
"&&"                               return "&&"

// Bitwise operators
"|"                               return "|"
"&"                               return "&"
"^"                               return "^"

// Unary operators
"!"                                return "!"

// End of file
<<EOF>>                            return 'EOF'
// Anything else is an invalid character
.                                  return 'Invalid_character'

%%

/lex

// BISON GRAMMAR

// Start symbol
%start Program

// Resolving the dangling else shift-reduce comflict in the grammar
// using precedence. See the ConditionalStatement rule.
%left NO_ELSE
%left "ELSE"

%%

/*
 * Root of the parse tree - the start symbol
 *
 * Program consists of a list of ProgramElements ending with EOF
 * ProgramElements are special (and different from Statement), because they
 * may contain function declarations. Function declarations cannot appear anywhere
 * else, and this ensures that functions are declared only in the top-most scope.
 */
Program
    : ProgramElements EOF
        {
            // false: ensure this ListASTNode does not enter nested scope (stay in global)
            $$ = new PARAPPLE.ast.ListASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), $1, false);
            return $$;
        }
    ;

ProgramElements
    : ProgramElements ProgramElement
        {
            $$ = $1.concat($2);
        }
    |
        {
            $$ = [];
        }
    ;

ProgramElement
    : Statement
    | FunctionDeclaration
    ;

FunctionDeclaration
    : "FUNCTION" "IDENTIFIER" "(" ArgumentList ")" Block
      {
          $$ = new PARAPPLE.ast.FunctionASTNode(new PARAPPLE.ast.SourceLocation(@1, @6), $2, $4, $6);
      }
    ;

ArgumentList
    : // NOTE: this line is not just a comment; Empty argument list is allowed
    | ArgumentList "," Argument
      {
        $$ = $1.concat($3);
      }
    | Argument
      {
        $$ = [ $1 ];
      }
    ;

// Argument is just an identifier
Argument
    : "IDENTIFIER"
    ;

Statement
    : Block
    | SelectionStatement
    | IterationStatement
    | ExpressionStatement
    | BreakStatement
    | ContinueStatement
    | ReturnStatement
    ;

/*
 * Block of statements
 *
 * Simply a list of statements enclosed in curly braces.
 * Important: Parascript is block-scoped. The scope-logic is implemented in
 * ListASTNode (with the use of identifierTable).
 */
Block
      : "{" StatementList "}"
          {
            // ListASTNode, true: enter nested scope
            $$ = new PARAPPLE.ast.ListASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $2, true);
          }
      ;

/*
 * IF statement
 */
SelectionStatement
      : "IF" "(" Expression ")" Statement %prec NO_ELSE
        {
          $$ = new PARAPPLE.ast.IfASTNode(new PARAPPLE.ast.SourceLocation(@1, @5), $3, $5, null);
        }
      | "IF" "(" Expression ")" Statement "ELSE" Statement
        {
          $$ = new PARAPPLE.ast.IfASTNode(new PARAPPLE.ast.SourceLocation(@1, @7), $3, $5, $7);
        }
      ;

BreakStatement
      : "BREAK" ";"
          {
            $$ = new PARAPPLE.ast.BreakASTNode(new PARAPPLE.ast.SourceLocation(@1, @2));
          }
      ;

ContinueStatement
      : "CONTINUE" ";"
          {
            $$ = new PARAPPLE.ast.ContinueASTNode(new PARAPPLE.ast.SourceLocation(@1, @2));
          }
      ;

ReturnStatement
      : "RETURN" ";"
          {
              $$ = new PARAPPLE.ast.ReturnASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), null);
          }
      | "RETURN" Expression ";"
          {
              $$ = new PARAPPLE.ast.ReturnASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), $2);
          }
      ;

IterationStatement
    : ForStatement
    | WhileStatement
    ;

ForStatement
    : "FOR" "(" ExpressionStatement Expression ";" Expression ")" Statement
      {
        $$ = new PARAPPLE.ast.ForASTNode(new PARAPPLE.ast.SourceLocation(@1, @8),  $3, $4, $6, $8);
      }
    ;

WhileStatement
    : "WHILE" "(" Expression ")" Statement
        {
          $$ = new PARAPPLE.ast.WhileASTNode(new PARAPPLE.ast.SourceLocation(@1, @5), $3, $5);
        }
    ;

ExpressionStatement
    : ";"
      {
        $$ = new PARAPPLE.ast.EmptyASTNode();
      }
    | VarStatement
    | ExpressionNoMapLiteral ";"
        {
          // Expression statements need to be followed by a POP instructions, that is
          // why we need to nest the expression node under an expression statement node.
          $$ = new PARAPPLE.ast.ExpressionStatementASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1);
        }
    ;

VarStatement
    : "VAR" DeclarationList ";"
        {
          // false: do not enter nested scope
          $$ = new PARAPPLE.ast.ListASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $2, false);
        }
    ;

DeclarationList
    : Declaration
      {
        $$ = [ $1 ];
      }
    | DeclarationList "," Declaration
      {
        $$ = $1.concat($3)
      }
    ;

Declaration
    : "IDENTIFIER"
        {
          $$ = new PARAPPLE.ast.VarASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1, null);
        }
    | "IDENTIFIER" "=" Expression
        {
          $$ = new PARAPPLE.ast.VarASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3);
        }
    ;

StatementList
     : StatementList Statement
          {
              $$ = $1.concat($2);
          }
     |
          {
              $$ = [];
          }
      ;

ExpressionList
    : // NOTE: this line is not just a comment; Empty expression list is allowed
    | ExpressionList "," Expression
        {
          $$ = $1.concat($3);
        }
    | Expression
        {
          $$ = [ $1 ];
        }
    ;

/*
 * Expressions
 */
Expression
    : ExpressionNoMapLiteral
    | MapLiteral
    ;

ExpressionNoMapLiteral
    : AssignmentExpression
    ;

AssignmentExpression
    : TernaryExpression
    | "IDENTIFIER" AssignmentOperator Expression
      {
          $$ = new PARAPPLE.ast.IdentifierAssignmentASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    | MemberExpression AssignmentOperator Expression
      {
          $$ = new PARAPPLE.ast.MemberAssignmentASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

AssignmentOperator
    : "="
    | "ARITHMETIC_ASSIGNMENT_OPERATOR"
    ;

TernaryExpression
    : LogicalOrExpression
    | LogicalOrExpression "?" LogicalOrExpression ":" TernaryExpression
      {
        $$ = new PARAPPLE.ast.TernaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @5), $1, $3, $5);
      }
    ;

LogicalOrExpression
    : LogicalAndExpression
    | LogicalOrExpression "||" LogicalAndExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

LogicalAndExpression
    : BitwiseOrExpression
    | LogicalAndExpression "&&" BitwiseOrExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

BitwiseOrExpression
    : BitwiseAndExpression
    | BitwiseOrExpression "|" BitwiseAndExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

BitwiseAndExpression
    : BitwiseXorExpression
    | BitwiseAndExpression "&" BitwiseXorExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

BitwiseXorExpression
    : EqualityExpression
    | BitwiseXorExpression "^" EqualityExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

EqualityExpression
    : RelationalExpression
    | EqualityExpression "LOGICAL_EQUALITY" RelationalExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

RelationalExpression
    : AddSubExpression
    | RelationalExpression "RELATIONAL_OPERATOR" AddSubExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

AddSubExpression
    : MulDivExpression
    | AddSubExpression AddSubOperator MulDivExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

AddSubOperator
    : "+"
    | "-"
    ;

MulDivExpression
    : UnaryExpression
    | MulDivExpression MulDivOperator UnaryExpression
      {
        $$ = new PARAPPLE.ast.BinaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3, $2);
      }
    ;

MulDivOperator
    : "*"
    | "/"
    | "%"
    ;

UnaryExpression
    : PostfixExpression
    | PrefixExpression
    | MemberExpression
    | PrimaryExpression
    | UnaryOperator UnaryExpression
      {
        $$ = new PARAPPLE.ast.UnaryExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), $1, $2);
      }
    ;

UnaryOperator
    : "-"
    | "+"
    | "!"
    ;

PostfixExpression
    : "IDENTIFIER" PostfixOperator
      {
        $$ = new PARAPPLE.ast.PostfixExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), $1, $2);
      }
    ;

PostfixOperator
    : "++"
    | "--"
    ;

PrefixExpression
    : PrefixOperator "IDENTIFIER"
      {
        $$ = new PARAPPLE.ast.PrefixExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @2), $2, $1);
      }
    ;

PrefixOperator
    : "++"
    | "--"
    ;

MemberExpression
    : PrimaryExpression "[" Expression "]"
      {
        $$ = new PARAPPLE.ast.MemberExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @4), $1, $3, true);
      }
    | MemberExpression "[" Expression "]"
      {
        $$ = new PARAPPLE.ast.MemberExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @4), $1, $3, true);
      }
    | PrimaryExpression "." "IDENTIFIER"
      {
        $$ = new PARAPPLE.ast.MemberExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3);
      }
    | MemberExpression "." "IDENTIFIER"
      {
        $$ = new PARAPPLE.ast.MemberExpressionASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3);
      }
    ;

PrimaryExpression
    : Literal
    | Identifier
    | FunctionCall
    | "(" Expression ")"
      {
        $$ = $2
      }
    ;

FunctionCall
    : "IDENTIFIER" "(" ExpressionList ")"
      {
        $$ = new PARAPPLE.ast.FunctionCallASTNode(new PARAPPLE.ast.SourceLocation(@1, @4), $1, $3);
      }
    ;

Identifier
  : "IDENTIFIER"
    {
      $$ = new PARAPPLE.ast.IdentifierASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1);
    }
  ;

Literal
    : BooleanLiteral
    | NumericLiteral
    | StringLiteral
    | ArrayLiteral
    | NullLiteral
    ;

BooleanLiteral
  : "BOOLEAN"
      {
        $$ = new PARAPPLE.ast.LiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1);
      }
  ;

NumericLiteral
  : "NUMERIC_LITERAL"
    {
      $$ = new PARAPPLE.ast.LiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1);
    }
  ;

StringLiteral
  : "STRING"
      {
        $$ = new PARAPPLE.ast.LiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), $1);
      }
  ;

ArrayLiteral
  : "[" + ExpressionList + "]"
    {
      $$ = new PARAPPLE.ast.ArrayLiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $2)
    }
  ;

MapLiteral
  : "{" + PropertyList + "}"
    {
      $$ = new PARAPPLE.ast.MapLiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $2)
    }
  ;

PropertyList
  : // This line is not just a comment; empty property list is allowed.
  | PropertyList "," Property
    {
      $$ = $1.concat($3);
    }
  | Property
    {
      $$ = [ $1 ];
    }
  ;

Property
  : "IDENTIFIER" ":" Expression
    {
      $$ = new PARAPPLE.ast.PropertyASTNode(new PARAPPLE.ast.SourceLocation(@1, @3), $1, $3);
    }
  ;

NullLiteral
  : "NULL"
      {
        $$ = new PARAPPLE.ast.LiteralASTNode(new PARAPPLE.ast.SourceLocation(@1, @1), null);
      }
  ;

%%
