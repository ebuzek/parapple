export class Error {

  constructor(message) {
    this.message = message;
  }

}

export class CodeError extends Error {

  constructor(message, location) {
    super(message);
    this.location = location;
  }

}

export class InternalError extends Error {

  constructor(message, exception) {
    super(message);
    console.error("[InternalError] " + message + "", exception);
  }

}

export class RuntimeError extends CodeError {

  constructor(message, agent, instruction, error) {
    super(message, instruction.location);
    this.agent = agent;
    this.instruction = instruction;
    this.error = error;
  }

}
