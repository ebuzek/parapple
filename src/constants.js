var config = require('../config')

function getConfig() {
    return process.env.NODE_ENV === 'production'
        ? config.build
        : config.dev;
}

function staticPath(suffix) {
  var _config = getConfig();
  return _config.assetsPublicPath + _config.assetsSubDirectory + suffix;
}

export default {
    APP_ROOT: getConfig().assetsPublicPath,
    
    TUTORIALS_PATH: staticPath("/tutorials/"),
    PACKAGES_PATH: staticPath("/packages/"),

    /* Simulation constants */
    TICK_DURATION: 20, // Duration of one tick
    TICK_COST: 1 // Penalty subtracted in one tick (relevant for cost-based schedulers)
}