import AgentType from 'runtime/agentType.js';
import Agent from 'runtime/agent.js';
import global from 'utils/global.js';
import schedulerFactory from 'runtime/scheduler/SchedulerFactory.js';
import proxy from 'components/console/consoleProxy.js';
import constants from 'constants.js';
import mmProxy from 'components/utils/modalMessage/modalMessageProxy'
import instructionCost from 'runtime/instructionCost.js';

/**
 * Session
 * 
 * Object for maintaining current workbench state: currently loaded agent types and agents,
 * their source codes, and compiled instructions lists.
 */
export default class Session {

    constructor(tutorial, config) {
        this.tutorial = tutorial;
        this.config = config;

        // Map agentTypeName -> { AgentType }
        var agentId = 0;

        this.agentMap = {};
        this.agentNames = [];
        for (let agentDefinition of this.config.agents) {
            if (!this.agentMap[agentDefinition.agentType.name]) {
                // Instantiate object for this agent type and add a key to the map
                this.agentMap[agentDefinition.agentType.name] = new AgentType(agentDefinition.agentType);
            }
            let agentType = this.agentMap[agentDefinition.agentType.name];
            let agent = new Agent(agentDefinition, agentType, agentId++);
            this.agentMap[agentDefinition.agentType.name].addAgent(agent);
            this.agentNames.push(agentDefinition.name);
        }

        this.agentTypeNames = [];
        for (let agentType of this.config.agentTypes) {
            this.agentTypeNames.push(agentType.name);
            if (!this.agentMap[agentType.name] || !this.agentMap[agentType.name].agents.length < 1) {
                proxy.warn("System", "Tutorial config probably wrong; agentType " + agentType.name + " has no associated agents.");
            }
        }

        if (Object.keys(this.agentMap).length < 1) {
            proxy.error("System", "Tutorial config probably wrong; there were no agents created...");
        }

        this.scheduler = null;

        this.showScheduler = true;

        // Map packageName -> function of callbacks to execute after every tick in lockstep
        this.lockstepCallbacks = {};

        // Make session available for external API...
        global.publish(this, "session");
    }

    initialize() {
        instructionCost.setConstantProfile();
        this.tutorial.initialize(this);
        this.tutorial.reset(this);
        proxy.info("System", "Loaded and initialized tutorial '" + this.tutorial.definition.name + "'");
    }

    /**
     * Setup - create Scheduler, prepare for simulation.
     * 
     * Take current compiled code for all agent types and create
     * scheduler and executors. Then initialize simulation to start state.
     */
    setup() {
        if (this.scheduler) {
            this.scheduler.cleanup();
        }

        for (let agentTypeName of this.agentTypeNames) {
            let agentType = this.agentMap[agentTypeName];
            if (!agentType.isCompiled()) {
                if (!agentType.compile()) {
                    // Compilation problem
                    return false;
                }
            }
        }
        // All agent types are compiled, create scheduler
        this.scheduler = schedulerFactory.makeScheduler(this.config.schedulerType, this);
        this.reset();

        proxy.debug("System", "Initialized session with " + this.scheduler.type.name);

        return true;
    }

    reset() {
        this.scheduler.reset();
        //var t0 = performance.now();
        this.tutorial.reset();
        //var t = performance.now() - t0;
        //proxy.debug("System", "Tutorial reset in " + t + "ms.");
    }

    setSpeed(value) {
        var factor = 1;
        switch(value) {
            case "Max": factor = 0; break;
            case "8x": factor = 1/8; break;
            case "4x": factor = 1/4; break;
            case "2x": factor = 1/2; break;
            case "1/2x": factor = 2; break;
            case "1/4x": factor = 4; break;
            case "1/8x": factor = 8; break;
            default: factor = 1;
        }
        this.tickDuration = constants.TICK_DURATION * factor;
        proxy.debug("System", "Simulation speed: " + value + " (tick duration: " + this.tickDuration + "ms)");
    }

    tutorialFinished(status) {
        var message = status.message + "<br/><br/>Number of steps: <strong>" + this.scheduler.elapsedSteps + "</strong>";
        if (status.solved) {
            mmProxy.success({
                title: "Great success ;)",
                body: message 
            });
        } else {
            mmProxy.warning({
                title: "Fail :'(",
                body: message
            });
        }
    }

    tutorialUnsolved() {
        mmProxy.warningAlert("Your programs have finished, but the assignment remains unsolved... Try again ;)", "Tutorial unsolved");
    }

    /**
     * Destroy
     * 
     * Cleanup tutorial and all dependencies recursively.
     */
    destroy() {
        if (this.scheduler) {
            this.scheduler.cleanup();
        }
        function cleanupRecursive(_package) {
            if (_package.cleanup) {
                _package.cleanup();
            }
            // Cleanup dependencies recursively
            if (_package.dependencies) {
                for (let d of Object.values(_package.dependencies)) {
                    cleanupRecursive(d);
                }
            }
        }
        cleanupRecursive(this.tutorial);
    }

}