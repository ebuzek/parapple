import { RuntimeError } from 'common/errors.js';
import StandardScheduler from 'runtime/scheduler/standardScheduler.js';
import SchedulerType from 'runtime/scheduler/types.js';
import constants from 'constants.js';

export default class RandomScheduler extends StandardScheduler {

    constructor(session) {
        super(session);
        this.type = SchedulerType.random;
    }

    makeRound() {
        super.makeRound();
        // Adopted from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
        function shuffle(a) {
            for (let i = a.length; i; i--) {
                let j = Math.floor(Math.random() * i);
                [a[i - 1], a[j]] = [a[j], a[i - 1]];
            }
        }
        shuffle(this.currentRound);
    }
}