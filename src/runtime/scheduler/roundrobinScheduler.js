import { RuntimeError } from 'common/errors.js';
import StandardScheduler from 'runtime/scheduler/standardScheduler.js';
import SchedulerType from 'runtime/scheduler/types.js';
import constants from 'constants.js';

export default class RoundrobinScheduler extends StandardScheduler {

    constructor(session) {
        super(session);
        this.type = SchedulerType.roundrobin;
    }

}