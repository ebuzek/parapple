export default {

    READY: {
        icon: "play-circle",
        name: "Ready",
        cssClass: ["success"]
    },
    PAUSED: {
        icon: "pause-circle",
        name: "Paused",
        cssClass: ["warning"]
    },
    RUNNING: {
        icon: "spinner",
        name: "Running",
        cssClass: ["info"]
    },
    DONE: {
        icon: "check",
        name: "Finished",
        cssClass: ["success"]
    },
    ERROR: {
        icon: "warning",
        name: "Error",
        cssClass: ["danger"]
    }

}