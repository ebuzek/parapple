import { RuntimeError } from 'common/errors.js';
import Scheduler from 'runtime/scheduler/scheduler.js';
import constants from 'constants.js';
import Stack from 'utils/stack.js';
import SchedulerType from 'runtime/scheduler/types.js';

export default class LockstepScheduler extends Scheduler {

    constructor(session) {
        if (session.agentTypeNames.length != 1) {
            throw "Lock-step scheduler may be used only for tutorials with single agent type!";
        }
        super(session);
        this.type = SchedulerType.lockstep;
    }

    reset() {
        // Map address -> list of waiting executors
        this.waitMap = {};
        this.waitStack = new Stack();
        this.currentRound = null;
        this.activeExecutors = this.executors;
        this.lastResult = null;
        super.reset();
    }

    run() {
        super.run();
        this.ensureRound();
        var self = this;
        function loop() {
            self.tick();
            if (self.isRunning()) {
                window.setTimeout(loop, self.session.tickDuration);
            }
        }
        loop();
    }

    tick() {
        var round = this.currentRound;
        while (round == this.currentRound) {
            this.step(this.nextExecutor);
            if (!this.isRunning()) {
                break;
            }
        }
    }

    lockstep(last) {
        // Invoke lock-step callbacks; these are functions hooked in by packages
        // that implement some lockstep-related functionality that requires certain
        // actions after each lock-step tick (i.e. destroy data that last a single tick...)
        for (let lockstepCallback of Object.values(this.session.lockstepCallbacks)) {
            lockstepCallback(last, this.activeExecutors);
        }

        // join, swap...

        if (last.id == "COND_JUMP" || last.id == "CALL") {
            // Conditional jump
            let jumped = [];
            let notJumped = [];
            for (let e of this.activeExecutors) {
                if (e.jumped) {
                    jumped.push(e);
                } else {
                    notJumped.push(e);
                }
            }
            if (notJumped.length == 0) {
                // Special case: all executors jumped over, nothing to do
                // Note: in the opposite case, when no executors jump (all enter the for/while/if)
                // an empty set on the wait stack is created (necessary for else/break/continue)
            } else {
                // Wait address: the instruction index where the executors will eventually join
                // It is the target address for COND_JUMP and the next instruction for function CALL
                let waitAddress = last.id == "COND_JUMP" ? last.params.address : (last.address + 1);
                // 'jumped' executors will wait in wait map (this list may be empty)
                this.wait(jumped, waitAddress);
                // 'not jumped' executors will be the next active set (cannot be empty)
                this.activeExecutors = notJumped;

                if (last.continueAddr) {
                    // COND_JUMP belongs to a for/while with CONTINUE addr
                    // Prepare empty wait set for some executors that may JUMP to continue
                    this.wait([], last.continueAddr);
                }
            }
        }

        // Join?
        if (this.areWaiting()) {

            let currentAddress = this.activeExecutors[0].ip;

            if (last.isBreak || last.isContinue || last.id == "RETURN") {
                // BREAK/CONTINUE JUMP or RETURN: join active executors with executors waiting on this address
                this.wait(this.activeExecutors, currentAddress);
                this.activeExecutors = this.pop(); // This may well return the executors that were just pushed
            } else if (last.isElse) {
                // ELSE JUMP - swap!
                let waiting = this.pop();
                this.wait(this.activeExecutors, currentAddress);
                this.activeExecutors = waiting;
            }

            // Executor list popped from the waiting set may be empty
            // Keep poppin' until there is a non-empty list
            while (this.activeExecutors.length == 0) {
                this.activeExecutors = this.pop();
            }

            currentAddress = this.activeExecutors[0].ip; // Set of active executors changed; this is the current address
            // Check if active executors caught up with some waiting set
            if (this.waitMap.hasOwnProperty(currentAddress)) {
                // YES -> JOIN the waiting set with active
                let waiting = this.pop();
                this.activeExecutors.push(...waiting);
            }
        }

    }

    areWaiting() {
        return this.waitStack.size() > 0;
    }

    // Add list of executors to the waiting stack (and waiting map)
    wait(executors, waitAddress) {
        if (!this.waitMap[waitAddress]) {
            // There are no executors waiting add this address, add element
            this.waitMap[waitAddress] = executors;
            // The wait stack stores objects to keep track of addresses of empty executor lists...
            this.waitStack.push({ executors, waitAddress });
        } else {
            // There are executors waiting add this address already, just join the two lists
            this.waitMap[waitAddress].push(...executors);
        }
    }

    // Return the latest (top-most) set of waiting executors
    pop() {
        let element = this.waitStack.pop();
        let executors = element.executors;
        let addr = element.waitAddress;
        delete this.waitMap[addr];
        return executors;
    }

    step(executor) {
        let result = super.step(executor);
        if (this.currentRound.length == 0 && result && result.instruction) {
            this.lockstep(result.instruction);
        }
        if (result) {
            this.iterate();
        } 
    }

    debugTick() {
        super.debugTick();
        super.run();
        this.tick();
        this.highlightNext();
        super.pause();
    }

    debugStep() {
        super.debugStep();
        this.step(this.nextExecutor);
        this.highlightNext();
    }

    getNextExecutor() {
        this.ensureRound();
        return this.currentRound.pop();
    }

    ensureRound() {
        if (!this.currentRound || this.currentRound.length == 0) {
            this.makeRound();
        }
    }

    makeRound() {
        this.currentRound = [...this.activeExecutors]; // Array copy
    }

}