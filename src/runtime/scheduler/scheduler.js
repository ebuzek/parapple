import Executor from 'runtime/executor.js';

import SCHEDULER_STATE from 'runtime/scheduler/states.js';
import { InternalError } from 'common/errors.js';

/**
 * Scheduler
 * 
 * This is an abstract class to define contract for all schedulers
 */
export default class Scheduler {

    constructor(session) {
        this.session = session;
        this.state = SCHEDULER_STATE.READY;
        this.doneExecutors = 0;
        this.type = null;
        this.createExecutors();
        this.reset();
    }

    createExecutors() {
        this.executors = [];
        this.executorMap = {};
        for (let agentTypeName of this.session.agentTypeNames) {
            let agentType = this.session.agentMap[agentTypeName];
            for (let agentName of agentType.agentNames) {
                let agent = agentType.agents[agentName];
                this.addExecutor(agent.createExecutor());
            }
        }
    }

    addExecutor(executor) {
        this.executors.push(executor);
        this.executorMap[executor.agent.name] = executor;    
    }

    // Reset to initial state
    reset() {
        this.lowlight();
        this.breakpoint = false;
        this.elapsedSteps = 0;
        this.accumulatedCost = 0;
        this.exception = null;
        this.state = SCHEDULER_STATE.READY;
        for (let e of this.executors) {
            e.reset();
        }
        this.doneExecutors = 0;
        // Set last and next executors
        this.nextExecutor = null;
        this.iterate();
    }

    // Single step of one executor
    step(executor) {
        if (this.isRunning() && executor.breakpoint() && !this.breakpoint) {
            this.breakpoint = true;
            this.pause();
            executor.highlight(true); // highlight next instruction
            return null;
        } else {
            this.breakpoint = false;
        }
        var result = executor.performNext();
        this.elapsedSteps++;
        this.lastExecutor = executor;
        if (result.error) {
            this.error(result.error);
            return result;
        }
        if (result.done) {
            this.doneExecutors++;
            if (this.doneExecutors >= this.executors.length) {
                this.done();
            }
        }
        if (((executor.last && executor.last.id=='CALL_PACKAGE') || this.isDone()) && !result.error) {
            try {
                this.checkFinished();
            } catch (e) {
                this.error(new InternalError("Tutorial Error: exception while checking tutorial status", e));
            }
        }
        return result;
    }

    iterate() {
        this.lastExecutor = this.nextExecutor;
        this.nextExecutor = this.getNextExecutor();
    }

    // Must be overriden
    getNextExecutor() {
        throw "Unimplemented";
    }

    // Check if the tutorial has completed
    // This should be called after each step
    checkFinished() {
        let status = this.session.tutorial.getStatus(this.isDone());
        if (status && status.done) {
            // Stop execution
            this.done();
            // Show modal
            this.session.tutorialFinished(status);
            return;
        }
        if (this.isDone()) {
            // Tutorial not finished/failed, but simulation is finished...
            this.session.tutorialUnsolved();
        }
    }

    done() {
        this.nextExecutor = null;
        this.exception = null;
        this.state = SCHEDULER_STATE.DONE;
        this.lowlight();
    }

    // Resume execution
    run() {
        this.exception = null;
        this.lowlight();
        this.state = SCHEDULER_STATE.RUNNING;
    }

    // Pause execution
    pause() {
        this.exception = null;
        this.state = SCHEDULER_STATE.PAUSED;
    }

    error(e) {
        this.exception = e;
        this.state = SCHEDULER_STATE.ERROR;
        this.lastExecutor.highlight(true);
    }

    // Single instruction of all agents (one round until the same agent)
    debugTick() {
        
    }

    // Single instruction of a single agent
    debugStep() {
        
    }

    /*
     * State helper getters
     */
     isReady() {
         return this.state == SCHEDULER_STATE.READY;
     }
     isPaused() {
         return this.state == SCHEDULER_STATE.PAUSED;
     }
     isRunning() {
         return this.state == SCHEDULER_STATE.RUNNING;
     }
     isDone() {
         return this.state == SCHEDULER_STATE.DONE;
     }
     isError() {
         return this.state == SCHEDULER_STATE.ERROR;
     }

     highlightNext() {
        if (this.nextExecutor) {
            this.nextExecutor.highlight(true);
        }
     }

     lowlight() {
         for (let e of this.executors) {
             e.lowlight();
         }
     }

     cleanup() {
         this.pause();
         if (this.timeout) {
             window.clearTimeout(this.timeout);
         }
         for (var e of this.executors) {
             e.cleanup();
         }
     }

}