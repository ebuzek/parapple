import { RuntimeError } from 'common/errors.js';
import Scheduler from 'runtime/scheduler/scheduler.js';
import constants from 'constants.js';

export default class StandardScheduler extends Scheduler {

    run() {
        super.run();
        var self = this;
        function loop() {
            self.tick();
            if (self.isRunning()) {
                self.timeout = window.setTimeout(loop, self.session.tickDuration);
            }
        }
        loop();
    }

    tick() {
        var round = this.currentRound; // Get reference to the current round
        while (round == this.currentRound) {
            this.step(this.nextExecutor);
            if (!this.isRunning()) {
                break;
            }
        }
    }

    step(executor) {
        if (executor.penalty >= 1) {
            executor.penalty = Math.max(0, executor.penalty - constants.TICK_COST);
        }
        while (!executor.done && executor.penalty < 1) {
            var result = super.step(executor);
            if (result && result.cost) {
                executor.penalty = Math.round(result.cost);
                this.accumulatedCost += result.cost;
            }
            if (!this.isRunning()) {
                break;
            }
            if (this.isError()) {
                return;
            }
        }
        if (executor.done || executor.penalty >= 1) {
            this.iterate();
        }
    }

    debugTick() {
        super.debugTick();
        super.run();
        this.tick();
        super.pause();
    }

    debugStep() {
        super.debugStep();
        this.step(this.nextExecutor);
        this.highlightNext();
    }

    getNextExecutor() {
        this.ensureRound();
        return this.currentRound.pop();
    }

    ensureRound() {
        if (!this.currentRound || this.currentRound.length == 0) {
            this.makeRound();
        }
    }

    makeRound() {
        this.currentRound = [];
        for (let e of this.executors) {
            if (!e.done) {
                this.currentRound.push(e);
            }
        }
        this.currentRound.reverse();
    }

    reset() {
        this.currentRound = [];
        super.reset();
    }

}