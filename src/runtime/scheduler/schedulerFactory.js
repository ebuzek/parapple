import RandomScheduler from 'runtime/scheduler/randomScheduler.js';
import RoundrobinScheduler from 'runtime/scheduler/roundrobinScheduler.js';
import LockstepScheduler from 'runtime/scheduler/lockstepScheduler.js';
import SchedulerType from 'runtime/scheduler/types.js';
import proxy from 'components/console/consoleProxy.js';

class SchedulerFactory {

    makeScheduler(schedulerType, session) {

        try {
            switch(schedulerType) {
                case SchedulerType.roundrobin.key:
                    return new RoundrobinScheduler(session);
                case SchedulerType.random.key:
                    return new RandomScheduler(session);
                case SchedulerType.lockstep.key:
                    return new LockstepScheduler(session);
                default:
                    proxy.warn("System", "Unknown scheduler type: " + schedulerType);
            }
        } catch(e) {
            proxy.error("System", "Error instantiating scheduler [" + schedulerType + "]: " + e);
        }
        proxy.info("System", "Defaulting to round-robin scheduler...");
        return new RoundrobinScheduler(session);
    }

}

export default new SchedulerFactory();