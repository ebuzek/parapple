export default {

    roundrobin: {
        name: "Round-robin Scheduler",
        description: "Executors are invoked in round-robin; instruction cost is accumulated as penalty which makes the executor skip cycles.",
        icon: "list-ol",
        key: "roundrobin"
    },
    random: {
        name: "Random Scheduler",
        description: "Executors are invoked in a random order (without repetition per cycle); instruction cost is accumulated as penalty which makes the executor skip cycles.",
        icon: "random",
        key: "random"
    },
    lockstep: {
        name: "Lock-step Scheduler",
        description: "All executors perform the same instruction in every cycle.",
        icon: "indent",
        key: "lockstep"
    }

}