/**
 * AgentType
 * 
 * Helper class containing all agents for given agent type,
 * its current source code, compiled code (if available).
 * 
 * Objects of class AgentType also contain the agentType definition
 * provided by the tutorial initializer.
 */
const STATE_NOT_COMPILED = {
  status: "NOT_COMPILED",
  icon: "edit",
  tabIcon: "pencil",
  message: "Not compiled."
}
const STATE_COMPILING = {
  status: "COMPILING",
  icon: "gears",
  tabIcon: "gears",
  message: "Compiling."
}
const STATE_COMPILED = {
  status: "COMPILED",
  icon: "check",
  tabIcon: "check",
  message: "Compiled."
}
const STATE_ERROR = {
  status: "ERROR",
  icon: "warning",
  tabIcon: "warning",
  message: "Error."
}

import { DEFAULT_AGENT_COSTS } from 'runtime/instructionCost.js';
import Compiler from 'parascript/compiler.js';
import mmProxy from 'components/utils/modalMessage/modalMessageProxy.js';

export default class AgentType {

    /**
     * @param definition Agent type Definition as provided by tutorial
     */
    constructor(definition) {    
        this.definition = definition;
        this.name = definition.name;
        // Map agentName -> agent
        this.agents = {};
        this.agentNames = [];
        // Latest source attempted to compile
        this.source = null;
        // Latest compiled paracode
        this.instructions = null;

        // Map rowNumber -> instruction (used for placing breakpoints to instructions)
        this.sourceMap = {};
        this.breakpointCount = 0;

        // Prepare API & Instruction costs
        this.prepareAgentAPI();
        this.prepareAgentCosts()
        // Current state of source code
        this.stateNotCompiled();
    }

    addAgent(agent) {
        this.agents[agent.name] = agent;
        this.agentNames.push(agent.name);
    }

    setCodearea(codearea) {
        this.codearea = codearea;
    }

    /**
     * Compile parascript source code with agent API to instructions
     * @param source 
     * @throws CodeError
     */
    compile(source) {
       this.removeBreakpoints();
       this.codearea.lowlightAgent();
       this.stateCompiling();
       var compiler = new Compiler();
       this.source = this.codearea.getSource();;
       try {
         this.instructions = compiler.compile(this.source, this);
         this.makeSourceMap();
         this.stateCompiled();
         return true;
       } catch(e) {
         this.stateError(e);
         this.codearea.setError(e);
         this.focus();
         mmProxy.errorAlert(e.message, "Error compiling code for  '" + this.name + "':");
         return false;
       }
    }

    focus() {
       this.codearea.focus();
    }

    toggleBreakpoint(row) {
      if (this.state == STATE_COMPILED) {
        var instruction = this.sourceMap[row];
        if (!instruction) {
          // Instruction not in sourceMap; cannot place breakpoint on this line;
          return;
        }
        if (instruction.breakpoint) {
          instruction.breakpoint = false;
          this.codearea.$refs.aceEditor.clearBreakpoint(row);
          this.breakpointCount--;
        } else {
          instruction.breakpoint = true;
          this.codearea.$refs.aceEditor.setBreakpoint(row);
          this.breakpointCount++;
        }
      }
    }

    removeBreakpoints() {
      if (this.breakpointCount > 0) {
        if (this.instructions) {
          for (var i of this.instructions) {
            i.breakpoint = false;
          }
        }
        this.codearea.$refs.aceEditor.removeBreakpoints();
        this.breakpointCount = 0;
      }
    }

    /*
     * States
     */
    stateNotCompiled() {
        this.state = STATE_NOT_COMPILED;
    }
    stateError(codeError) {
        this.state = STATE_ERROR;
        this.state.message = codeError.message;
    }
    stateCompiled() {
        this.state = STATE_COMPILED;
    }
    stateCompiling() {
        this.state = STATE_COMPILING;
    }
    isCompiled() {
        return this.state == STATE_COMPILED;
    }

   /*
    * Pre-process agent API to package functions and constants
    */
  prepareAgentAPI() {
    let packageFunctions = {};
    let packageConstants = {};
    for (let api of this.definition.api) {
      $.extend(packageFunctions, api.functions);
      $.extend(packageConstants, api.constants);
    }
    this.api = {
      packageConstants,
      packageFunctions
    }
  }

  /*
   * Initialize map of instructions to cost functions used for this agent type
   */
  prepareAgentCosts() {
    this.costs = $.extend(DEFAULT_AGENT_COSTS , this.definition.costs);
    console.log("COSTS", this.costs);
  }

    /**
     * Prepare map of row lines to instructions used for breakpoints
     */
    makeSourceMap() {
      this.sourceMap = {};
      for (var i of this.instructions) {
        if (i.location && this.sourceMap[i.location.startLine - 1] == undefined) {
          this.sourceMap[i.location.startLine - 1] = i;
        }
      }
    }

}