import constants from 'constants.js';
const DEFAULT_UNIT_COST = constants.TICK_COST;
const RANDOM_COST_COEF = 5;

class CostProfile {
    
    unit() {}

    free() { return 0; }
}

class ConstantCostProfile extends CostProfile {

    unit() { return DEFAULT_UNIT_COST; }

}

class RandomCostProfile extends CostProfile {

    unit() { return Math.random() * DEFAULT_UNIT_COST * RANDOM_COST_COEF; }

}

class AgentCostProfile extends ConstantCostProfile {

    unit(agent) { return agent.instructionCost + Math.random() * agent.instructionCost * DEFAULT_UNIT_COST }

}

class InstructionCost {

    constructor() {
        this.setConstantProfile();
    }

    setConstantProfile() {
        this.profile = new ConstantCostProfile();
    }

    setRandomProfile() {
        this.profile = new RandomCostProfile();
    }

    setAgentProfile() {
        this.profile = new AgentCostProfile();
    }

    unit(agent) {
        return this.profile.unit(agent);
    }

    free(agent) {
        return this.profile.free(agent);
    }

}

// Make instructionCost object global so that API functions can invoke it
var instructionCost = new InstructionCost();
import global from 'utils/global.js';
global.publish(instructionCost, "instructionCost");
export default instructionCost;

const COST_PROXY = {

    free: function(agent) {
        return instructionCost.free(agent);
    },

    unit: function(agent) {
        return instructionCost.unit(agent);
    }

}

export const DEFAULT_AGENT_COSTS = {
  HALT: COST_PROXY.free,

  JUMP: COST_PROXY.unit,
  COND_JUMP: COST_PROXY.unit,

  CALL: COST_PROXY.unit,
  CALL_PACKAGE: COST_PROXY.unit,
  RETURN: COST_PROXY.unit,

  DECLARE: COST_PROXY.free,
  ASSIGN: COST_PROXY.unit,

  PUSH: COST_PROXY.free,
  POP: COST_PROXY.free,
  PUSH_LITERAL: COST_PROXY.free,

  ARRAY: COST_PROXY.free, 
  MAP: COST_PROXY.free,
  MEMBER: COST_PROXY.free,
  MEMBER_ASSIGN: COST_PROXY.free,

  BINARY_OP: COST_PROXY.unit,
  UNARY_OP: COST_PROXY.unit
}