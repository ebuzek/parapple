import Stack from 'utils/stack.js'

/**
 * ValueStack
 * 
 * Extension of Stack used for the main value stack
 * in the executor. It takes care of undefined -> conversion
 */
export default class ValueStack extends Stack {

    push(e) {
        // Convert undefined to null before pushing onto the stack
        if (e === undefined) {
            e = null;
        }
        super.push(e);
    }

}