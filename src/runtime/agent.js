
import Executor from 'runtime/executor.js';

export default class Agent {

    constructor(definition, agentType, agentId) {  
        this.definition = definition;
        this.name = definition.name;
        this.agentType = agentType;
        this.definition.agentId = agentId;
    }

    createExecutor() {
        this.executor = new Executor(this);
        return this.executor;    
    }

    highlight() {
        if (this.executor) {
            this.executor.highlight();
        }
    }

}