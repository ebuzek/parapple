import Stack from 'utils/stack.js';
import ValueStack from 'runtime/valueStack.js';
import Memory from 'runtime/memory.js';
import proxy from 'components/console/consoleProxy.js';

export default class Executor {

    constructor(agent) {
        this.agent = agent;
        this.instructions = agent.agentType.instructions;
        this.reset();
    }

    reset() {
        // Instruction pointer
        this.ip = 0;
        this.elapsedSteps = 0;
        this.accumulatedCost = 0;
        this.done = false;
        this.stack = new ValueStack();
        this.addressStack = new Stack();
        this.memory = new Memory();
        this.error = null;
        this.penalty = 0; // Used for cost-based schedulers
        // Initialize next and last instructions
        this.next = null;
        this.lastIp = null;
        this.iterate(); // next: [0]; last: null
    }

    performNext() {
        var instruction = this.next;
        this.elapsedSteps++;
        this.lastIp = this.ip;
        try {
            var cost = instruction.perform(this);
            this.accumulatedCost += cost;
            // Set last instruction and load next
            this.iterate();
            return {
                instruction,
                cost,
                done: this.done,
                error: false
            }
        } catch (e) {
            // Runtime error
            console.error(e);
            proxy.error(this.agent.name, "Runtime Error: " + e.message);
            this.error = e;
            this.done = true;
            return {
                instruction,
                error: e,
                done: true
            }
        }
    }

    breakpoint() {
        return this.next && this.next.breakpoint;
    }

    iterate() {
        this.last = this.next;
        this.next = this.instructions[this.ip];
    }

    /**
     * Highlight next or last instruction in debugger
     * 
     * -Give focus to relevant editor tab
     * -Highlight instruction in editor
     */
    highlight(next) {
        if (next && this.next) {
            this.highlighted = true;
            this.agent.agentType.codearea.highlightAgent(this.agent, this.next);
        } else if (this.last) {
            this.highlighted = true;
            this.agent.agentType.codearea.highlightAgent(this.agent, this.last);
        }
    }

    lowlight() {
        if (this.highlighted) {
            this.agent.agentType.codearea.lowlightAgent();
        }
    }

    cleanup() {
        this.lowlight();
        this.done = true;
        this.agent = null;
        this.instructions = null;
    }

    /*
     * Redeem Promise from Package Function Calls
     * 
     * To implement certain functionality (e.g. broadcast in lock-step), package
     * function calls don't return the result directly, but they only return
     * a function as a promise. The value is retrieved using this method, which
     * should be called when appropriate (typically at the end of a tick from
     * lock-step callback.)
     */
    redeemPromise() {
        // Check if the top of the value stack contains a function (the promise)
        let lastPush = this.stack.peek();
        if (lastPush && typeof(lastPush) == 'function') {
            // If yes, replace the top value on the stack
            this.stack.pop();
            // with the result of the function (the promised value)
            this.stack.push(lastPush());
        }
    }

}