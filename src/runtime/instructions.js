import { RuntimeError, InternalError } from 'common/errors.js'

export const INSTRUCTION_TYPES = {

  HALT: "HALT",

  JUMP: "JUMP",
  COND_JUMP: "COND_JUMP",

  CALL: "CALL", // User function call
  CALL_PACKAGE: "CALL_PACKAGE", // Package function call
  RETURN: "RETURN",

  DECLARE: "DECLARE", // Declare identifier
  ASSIGN: "ASSIGN", // Pop value and assign identifier

  PUSH: "PUSH", // Push identifier
  POP: "POP",
  PUSH_LITERAL: "PUSH_LITERAL", // Push literal

  ARRAY: "ARRAY", // Instantiate array
  MAP: "MAP", // Instantiate map
  MEMBER: "MEMBER", // Access member field (or array index)
  MEMBER_ASSIGN: "MEMBER_ASSIGN", // Assign member field (or array index)

  BINARY_OP: "BINARY_OP",
  UNARY_OP: "UNARY_OP"
}

export class Instruction {

  constructor(id, location) {
    this.id = id;
    this.location = location;
    this.params = {};
    this.cost = null; // Set in post-processing in code generation (ast.js)
  }

  /**
   * Method to execute this instruction.
   * This is invoked by the executor.
   * 
   * @param executor 
   */
  perform(executor) {
  }

}

export class HaltInstruction extends Instruction {

  constructor() {
    super(INSTRUCTION_TYPES.HALT, null);
  }

  perform(executor) {
    executor.done = true;
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class JumpInstruction extends Instruction {

  constructor(location, address) {
    super(INSTRUCTION_TYPES.JUMP, location);
    this.params.address = address;
  }

  setAddress(address) {
    this.params.address = address;
  }

  perform(executor) {
    executor.ip = this.params.address;
    return this.cost(executor.agent.definition);
  }

}

export class ConditionalJumpInstruction extends Instruction {

  /**
   * ConditionalJumpInstruction
   * 
   * Jump to address iff value on top of the stack is falsy.
   */
  constructor(location) {
    super(INSTRUCTION_TYPES.COND_JUMP);
  }

  setAddress(address) {
    this.params.address = address;
  }

  // Get the condition value
  getCondition(executor) {
    return executor.stack.pop();
  }

  perform(executor) {
    var condition = this.getCondition(executor);
    if (!condition) {
      executor.ip = this.params.address;
      executor.jumped = true; // Lockstep helper flag
    } else {
      executor.ip++;
      executor.jumped = false; // Lockstep helper flag
    }
    return this.cost(executor.agent.definition);
  }

}

export class ShortCircuitConditionalJumpInstruction extends ConditionalJumpInstruction {

  /**
   * ConditionalJumpInstruction used for short-circuit evaluation
   * 
   * @param negate Flip condition value
   */
  constructor(location, negate) {
    super(location);
    this.params.peek = true;
    this.params.negate = negate;
  }

  // Get the condition value
  getCondition(executor) {
    let condition = executor.stack.peek();
    if (this.params.negate) {
      condition = !condition;
    }
    return condition;
  }

}

export class ReturnInstruction extends Instruction {

  constructor(location) {
    super(INSTRUCTION_TYPES.RETURN, location);
  }

  perform(executor) {
    executor.memory.exit();
    // return value is already on top of main stack
    executor.ip = executor.addressStack.pop() + 1;
    return this.cost(executor.agent.definition);
  }

}

// Helper abstract class
export class IdentifierInstruction extends Instruction {

  constructor(id, location, identifier, global) {
    super(id, location);
    if (global === null || global === undefined) {
      throw new InternalError("Global must be specified for identifier " + identifier, location);
    }
    this.params.identifier = identifier;
    this.params.global = global;
  }

}

export class PushInstruction extends IdentifierInstruction {

  constructor(location, identifier, global) {
    super(INSTRUCTION_TYPES.PUSH, location, identifier, global);
  }

  perform(executor) {
    var value;
    if (this.params.global) {
      value = executor.memory.loadGlobal(this.params.identifier);
    } else {
      value = executor.memory.loadLocal(this.params.identifier);
    }
    executor.stack.push(value);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class PopInstruction extends Instruction {

  constructor(location) {
    super(INSTRUCTION_TYPES.POP, location);
  }

  perform(executor) {
    executor.stack.pop();
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class PushLiteralInstruction extends Instruction {

  constructor(location, value) {
    super(INSTRUCTION_TYPES.PUSH_LITERAL, location);
    this.params.value = value;
  }

  perform(executor) {
    executor.stack.push(this.params.value);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class ArrayInstruction extends Instruction {

  constructor(location, length) {
    super(INSTRUCTION_TYPES.ARRAY, location);
    this.params.length = length;
  }

  perform(executor) {
    // Pop correct number of elements off of the stack
    var array = executor.stack.popN(this.params.length);
    // Push the resulting array on top of the stack
    executor.stack.push(array);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class MapInstruction extends Instruction {

  constructor(location, properties) {
    super(INSTRUCTION_TYPES.MAP, location);
    this.params.properties = properties;
  }

  perform(executor) {
    // Pop correct number of elements off of the stack
    var values = executor.stack.popN(this.params.properties.length);
    // Create map
    var map = {};
    for (let i = 0; i < this.params.properties.length; i++) {
      map[this.params.properties[i]] = values[i]; 
    }
    executor.stack.push(map);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class AbstractMemberInstruction extends Instruction {

  constructor(instructionType, location, field) {
    super(instructionType, location);
    this.params.field = field;
  }

  getIndexable(executor) {
    var indexable = executor.stack.pop();
    if (indexable === null || indexable === undefined) {
      // NPE
      throw new RuntimeError("Null pointer access: Array or Map is not initialized.", this, executor.agent);
    }
    if (indexable.constructor.name != 'Array' && indexable.constructor.name != 'Object') {
      // Wrong type
      throw new RuntimeError("Illegal member access: expected Array or Map, but got " + typeof(indexable) + " instead.", this, executor.agent);
    }
    return indexable;
  }

}

export class MemberInstruction extends AbstractMemberInstruction {

  constructor(location, field) {
    super(INSTRUCTION_TYPES.MEMBER, location, field);
  }

  perform(executor) {
    if (this.params.field) {
      // Field access using .identifier notation
      executor.stack.push(this.getIndexable(executor)[this.params.field]);
    } else {
      // Index access using [expression]
      var index = executor.stack.pop();
      executor.stack.push(this.getIndexable(executor)[index]);
    }
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class MemberAssignmentInstruction extends AbstractMemberInstruction {

  constructor(location, field) {
    super(INSTRUCTION_TYPES.MEMBER_ASSIGN, location, field);
  }

  perform(executor) {
    let index;
    if (this.params.field) {
      // Field access using .identifier notation
      index = this.params.field;
    } else {
      index = executor.stack.pop();
    }
    // Member owner - the indexable (map/array)
    let indexable = this.getIndexable(executor);
    // The value to be assigned to the member field
    let value = executor.stack.peek();
    // Assignment
    indexable[index] = value;
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class DeclareInstruction extends IdentifierInstruction {

  constructor(location, identifier, global) {
    super(INSTRUCTION_TYPES.DECLARE, location, identifier, global);
  }

  perform(executor) {
    if (this.params.global) {
      executor.memory.storeGlobal(this.params.identifier, null);
    } else {
      executor.memory.storeLocal(this.params.identifier, null);
    }
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class AssignInstruction extends IdentifierInstruction {

  /**
   * AssignInstruction
   * 
   * @param {boolean} global global or local identifier
   * @param {boolean} peek Flag indicating whether to pop or peek the assigned value from the stack.
   */
  constructor(location, identifier, global, peek) {
    super(INSTRUCTION_TYPES.ASSIGN, location, identifier, global);
    // For assignment in ExpressionStatement, we want to keep the assigned value
    // on the stack (peek). In other cases, the value should popped from the stack.
    // See issue #47.
    if (peek) {
      this.getValue = function(executor) {
        return executor.stack.peek(); 
      }
    } else {
      this.getValue = function(executor) {
        return executor.stack.pop();
      }
    }
  }

  perform(executor) {
    var value = this.getValue(executor);
    if (this.params.global) {
      executor.memory.storeGlobal(this.params.identifier, value);
    } else {
      executor.memory.storeLocal(this.params.identifier, value);
    }
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

function getBinaryOperation(operator) {
  // Arithmetic operators
  if (operator == "+") {                    
    return function(a, b) { return a + b } // +
  } else if (operator == "-") {
    return function(a, b) { return a - b } // -
  } else if (operator == "*") {
    return function(a, b) { return a * b } // Multiply
  } else if (operator == "/") {
    return function(a, b) { return a / b } // Divide
  } else if (operator == "%") {
    return function(a, b) { return a % b } // Modulo
  // Logical operators
  } else if (operator == "&&") {
    return function(a, b) { return a && b } 
  } else if (operator == "||") {
    return function(a, b) { return a || b } 
  // Comparison operators
  } else if (operator == "==") {
    return function(a, b) { return a == b } 
  } else if (operator == "!=") {
    return function(a, b) { return a != b } 
  } else if (operator == "===") {
    return function(a, b) { return a === b } 
  } else if (operator == "!==") {
    return function(a, b) { return a !== b } 
  } else if (operator == "<") {
    return function(a, b) { return a < b } 
  } else if (operator == ">") {
    return function(a, b) { return a > b } 
  } else if (operator == "<=") {
    return function(a, b) { return a <= b } 
  } else if (operator == ">=") {
    return function(a, b) { return a >= b } 
  // Bitwise Operators
  } else if (operator == "|") {
    return function(a, b) { return a | b } // Bitwise OR
  } else if (operator == "&") {
    return function(a, b) { return a & b } // Bitwise AND
  } else if (operator == "^") {
    return function(a, b) { return a ^ b } // Bitwise XOR
  } else if (operator == ">>") {
    return function(a, b) { return a >> b } // Right shift 
  } else if (operator == "<<") {
    return function(a, b) { return a << b } // Left shift
  }
  throw new InternalError("Unknown binary operator: " + operator);
}

export class BinaryOperationInstruction extends Instruction {

  constructor(location, operator) {
    super(INSTRUCTION_TYPES.BINARY_OP, location);
    this.params.operator = operator;
    this.operation = getBinaryOperation(operator);
  }

  perform(executor) {
    var value2 = executor.stack.pop();
    var value1 = executor.stack.pop();
    var result = this.operation(value1, value2);
    executor.stack.push(result);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

function getUnaryOperation(operator) {
  if (operator == "+") {
    return function(a) { return a } 
  } else if (operator == "-") {
    return function(a) { return -a } 
  } else if (operator == "!") {
    return function(a) { return !a } 
  } else if (operator == "--") {
    return function(a) { return --a } 
  } else if (operator == "++") {
    return function(a) { return ++a } 
  }
  throw new InternalError("Unknown unary operator: " + operator);
}

export class UnaryOperationInstruction extends Instruction {

  constructor(location, operator) {
    super(INSTRUCTION_TYPES.UNARY_OP, location);
    this.params.operator = operator;
    this.operation = getUnaryOperation(operator);
  }

  perform(executor) {
    var value = executor.stack.pop();
    var result = this.operation(value);
    executor.stack.push(result);
    executor.ip++;
    return this.cost(executor.agent.definition);
  }

}

export class FunctionCallInstruction extends Instruction {

  constructor(location, name, argCount) {
    super(INSTRUCTION_TYPES.CALL, location);
    this.params.name = name;
    this.params.argCount = argCount;
  }

  setAddress(address) {
    this.params.address = address;
  }

  perform(executor) {
    executor.memory.enter(this);
    executor.addressStack.push(executor.ip);
    executor.ip = this.params.address;
    return this.cost(executor.agent.definition);
  }

}


export class PackageFunctionCallInstruction extends Instruction {

  constructor(location, name, argCount, _function) {
    super(INSTRUCTION_TYPES.CALL_PACKAGE, location);
    this.params.name = name;
    this.params.argCount = argCount;
    this.params._function = _function;
  }

  perform(executor) {
    var args = executor.stack.popN(this.params.argCount);
    var result = this.params._function(executor.agent.definition, ... args);
    if (!result || isNaN(result.cost)) {
      // Runtime error: cost must be a number
      throw new RuntimeError("Package function must return Object containing cost.", this, executor.agent);
    }
    executor.stack.push(result.result);
    executor.ip++;
    return result.cost;
  }

}
