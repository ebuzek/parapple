import Stack from 'utils/stack.js';

export default class Memory {

    constructor() {
        // Initialize memory stack
        this.stack = new Stack();
        this.callStack = new Stack();
        // Push global scope onto the stack (initialized by default)
        this.stack.push({});
    }

    // Enter nested scope
    enter(jumpInstruction) {
        this.stack.push({});
        this.callStack.push(jumpInstruction);
    }

    // Exit nested scope
    exit() {
        this.stack.pop();
        this.callStack.pop();
    }

    localScope() {
        return this.stack.peek();
    }

    globalScope() {
        return this.stack.peekBottom();
    }

    storeLocal(key, value) {
        this.localScope()[key] = value;
    }

    loadLocal(key) {
        return this.localScope()[key];
    }

    storeGlobal(key, value) {
        this.globalScope()[key] = value;
    }

    loadGlobal(key) {
        return this.globalScope()[key];
    }

}