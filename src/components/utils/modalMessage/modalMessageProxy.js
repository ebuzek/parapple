var TYPE = {
    info: {
        icon: "info",
        type: "info"
    },
    success: {
        icon: "check",
        type: "success"
    },
    error: {
        icon: "remove",
        type: "danger"
    },
    warning: {
        icon: "warning",
        type: "warning"
    }
}

class ModalMessageProxy {
        
        constructor() {}

        info(options) {
            options.type = TYPE.info;
            this.show(options);     
        }

        success(options) {
            options.type = TYPE.success;
            this.show(options);  
        }

        error(options) {
            options.type = TYPE.error;
            this.show(options);  
        }

        warning(options) {
            options.type = TYPE.warning;
            this.show(options);  
        }

        show(options) {
            this.component.title = options.title;
            this.component.body = options.body;
            this.component.type = options.type || TYPE.info;
            this.component.show = true;   
        }

        alert(text, title, type) {
            this.component.alertText = text;
            this.component.alertTitle = title;
            this.component.alertType = type || TYPE.info;
            this.component.alertShow = true;
        }

        errorAlert(text, title) {
            this.alert(text, title, TYPE.error);
        }

        warningAlert(text, title) {
            this.alert(text, title, TYPE.warning);
        }

        successAlert(text, title) {
            this.alert(text, title, TYPE.success);
        }

        register(component) {
            this.component = component;
        }

    }

let proxy = new ModalMessageProxy();
export default proxy;

import global from 'utils/global.js';
global.publish(proxy, 'modalMessage');
