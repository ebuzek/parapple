> User Documentation

## What is Parapple
Parapple is trying to make learning parallel programming *easier*. In real life, parallel programming can be difficult and frustrating--many parallel platforms (such as MPI, CUDA, OpenCL...) have a pretty steep learning curve. This platform should help programmers with their first attempts with solving parallel problems. It should help beginners to focus on *how their think* about parallelism rather than worrying about the API and underlying architecture of sophisticated parallel platforms.   

## Get Started!
Just open the *first tutorial* and follow the instructions. Good luck!

### Basic Concepts
- **agents** execute the code you provide for them. Each agent executes the code separately, but they interact in a shared world. 
- **agent type** determines the available functions and constants for the given agent. Each agent has an associated type. You write one program per agent type. 
- **simulation** is the process of executing all agent programs. If the tutorial objective is achieved during the simulation, the tutorial is solved.
- **scheduler** determines the order in which agents execute their code in the simulation

#### Tutorial Objective
The tutorial assignment is always displayed in the *left panel*.

#### Completing a Tutorial
You have to write a program (in Parascript) for every agent type. In other words, you have to fill in some code in every tab in the lower part of the screen. Once you think it might work, or you just want to give it a shot, just hit *Compile & Run*. You will probably see some output in the console, and if your solution is correct, you you will see a modal message once your simulation completes.

#### Programming Help
The available functions and constants for each agent type can be found under the **Help** button in the top-right corner of the *coding area*.

#### Simulator GUI
The simulator is the main view. It consists of the following parts:

- **Control panel**: This is where you may compile your programs, launch the simulation, and control the simulation speed
- **Scheduler panel**: This panel is visible only when the simulation is running. It is the panel in the right side of the screen. It shows you all executors (one per agent) and it highlights the current (active) executor. For each executor, it shows you the last and next instructions.
- **Canvas**: the central element is the canvas used for visualizations. What is displayed in this area is completely dependent on the tutorial.

## Parascript Programming Language
The programming language used for programming the agents is called *Parascript*, and it is very similar to *JavaScript*. If you program in *simple* JavaScript, you should be more or less fine. There are several things that are different:
- no objects (no `new` keyword, prototyping)
- there is no `undefined`, all undefined values are `null`
- functions can be declared only in the top-most scope of the program
- all variables must be declared using `var`; variables are block-scoped
- semicolons are mandatory `;`

### Data types
Primitive data types:
- **null**
- **boolean**
- **number**
- **string**

Structured data types:
- **map** resembles very much JavaScript object. It is basically a key-value store. You may initialize it like this: `var map = { key: "values", foo: true }`. You can access map values with bracket notation: `map["key"]` or with dot notation: `map.key`. Keys and values can be of nay type.
- **array** is an ordered set of elements. Like map, it can contain mixed types including null. You may initialize arrays like this: `var array = [1, true, "three", null]`

### Variables
Variables must be declared using `var` statement. All variables are block-scoped.

### Expressions
Expressions work much like in JavaScript.

### IF-ELSE Statement
```
if (condition) {
    
} else {

}
```

### FOR Statement
```
for (var i = 0; i < 10; i++) {

}
```

### WHILE Statement
```
while (condition) {
    
}
```

### Functions
Functions may be declared only in the top-most block if the program. The **argument count** in the function declaration and in the function call must match.
```
function add(arg1, arg2) {
    return arg1 + arg2;
}

add(1, 2);
```

If a function does not contain an explicit `return` statement, it returns null.

## Schedulers
Scheduler determines the order in which the agents are executed. To be precise, it determines the interleaving order of the instructions of the compiled agent code.
There are three scheduling algorithms implemented:
- **round robin scheduler**
- **random scheduler** is the same as the round robin scheduler, but the agents are iterated in a random order (without repetition)
- **lock-step scheduler** simulates execution on a GPU. It ensures that in every simulation cycle, all agents execute exactly the same instruction. Code branching (IF and iteration statements) is solved by pausing/resuming certain executors. 

### Selecting a Scheduler
You may select the scheduler when loading the tutorial. Some tutorials allow only certain schedulers to be selected.

## Debugging
Debugging parallel programs can be difficult. Since the programs usually affect each other, they cannot be simply ran one-by-one and debugged separately.

In Parapple, you may always pause the simulation (pause button in the top bar). The tab of the *active agent* should get highlighted together with the exact code location (line of code highlighting the command that will be executed next.) Also, the *scheduler panel* (on the right) should be showing the active executor (agent).

### Placing a break point
To force the simulation to pause at an exact place in your code, you may insert a *breakpoint*. This is done by clicking the line number in the coding area of the program.

> Note: you may only place a breakpoint once the program is compiled! 

### Stepping
Once the simulation is paused (i.e., it hit a breakpoint), you may use the step-through button (top bar, next to pause) to run the simulation step-by-step. The instruction order will be exactly the same as when the simulation runs normally. 

### Memory Contents and Callstack
When the simulation is active, the following information can be found in the *right panel* in the code area
- memory contents
- call stack and stack contents
- list of compiled instructions