import Loader from 'resource-loader';
import Constants from '../../constants';

export default class TutorialPickerHelper {

    constructor() {
        /*
         * Maps tutorialName -> json
         */
        this.cache = {};
    }
    
    loadInstalledTutorials(callback) {
      var loader = new Loader();
      loader
        .add("index", Constants.TUTORIALS_PATH + "index.json")
        .load((loader, resources) => {
            if (resources.index) {
                callback(resources.index.data); 
            }
        });
    }

    loadTutorialIndex(tutorial, callback) {
      var loader = new Loader();
      loader
        .add("index", Constants.TUTORIALS_PATH + tutorial + "/index.json")
        .load((loader, resources) => {
            if (resources.index) {
                callback(resources.index.data); 
            }
        });
    }

    loadTutorialIntro(tutorial, intro, callback) {
      var loader = new Loader();
      if (intro.template) {
        loader.add("template", Constants.TUTORIALS_PATH + tutorial + "/" + intro.template);
      }
      if (intro.js) {
        loader.add("js", Constants.TUTORIALS_PATH + tutorial + "/" + intro.js);
      }
      loader.load((loader, resources) => {
            callback(resources.template, resources.js); 
      });
    }

}