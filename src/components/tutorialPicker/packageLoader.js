import Loader from 'resource-loader';
import Constants from '../../constants';

import proxy from 'components/console/consoleProxy.js';

class PackageLoader {

    constructor() {
        /*
         * Maps packageName -> { name, json, resources, packageConstructor, instance }
         */
        this.cache = {};
    }

    // Main method to load a tutorial and packages
    // Call this method to load a tutorial and all its dependencies
    load(tutorialName, callback) {
        proxy.debug("System", "Loading tutorial " + tutorialName + "...");
        this.remaining = 0;
        this.root = tutorialName;
        this.callback = callback;
        
        let cachedTutorial = this.loadMetadata(this.root);
        // Check if tutorial was already in cache
        if (cachedTutorial) {
            if (!cachedTutorial.instance) {
                // No instance; load dependencies and instantiate
                this.prepareDependencies();
            } else {
                // Instance in cache; invoke callback
                this.finished();
            }
        }
    }

    getBaseDir(packageName) {
        if (packageName == this.root) {
            return Constants.TUTORIALS_PATH;
        }
        return Constants.PACKAGES_PATH;
    }

    // Load Package metadata - load JSON files describing all packages
    loadMetadata(packageName) {
      if (this.cache[packageName]) {
          proxy.debug("System", " Ignoring " + packageName + " (already loaded/loading in progress)");
          return this.cache[packageName];
      }
      this.cache[packageName] = { name: packageName };
      this.remaining++;
      proxy.debug("System", " Loading package " + packageName + "...");
      var loader = new Loader();

      loader.onError.add((e) => {
            console.error(e);
            proxy.error("System", "Error loading index.json for " + packageName + ": " + e.message);
        });

      loader
        .add("index", this.getBaseDir(packageName) + packageName + "/index.json")
        .load((loader, resources) => {
            if (resources.index) {
                // Load dependencies recursively
                var json = resources.index.data;
                this.cache[packageName].json = json;
                for (let d of json.dependencies) {
                    this.loadMetadata(d);
                }
                this.remaining--;
                if (this.remaining <= 0) {
                    this.prepareDependencies();
                } 
            }
        });
    }

    // Sort dependencies
    prepareDependencies() {
        
        var self = this;
    
        // Using Kahn's algorithm (https://en.wikipedia.org/wiki/Topological_sorting)
        // the packages are sorted topologically using their dependency graph.
        // Then we can instantiate the dependency from the end of the topological sort.
        // This ensures that dependencies are instantiated in correct order.

        // Helper utility: recursively traverse the dependency graph and perform an action
        function visit(nodeName, operation) {
            let node = self.cache[nodeName];
            operation(node);
            for (let d of node.json.dependencies) {
                visit(d, operation);
            }
        }

        // For each node, create a set of incoming edges to the node
        visit(this.root, function(node) {
            for (let d of node.json.dependencies) {
                let child = self.cache[d];
                if (!child.incoming) {
                    // Initialize the set of incoming edges
                    child.incoming = { };
                }
                // Add the current node to the set of incoming edges
                child.incoming[node.name] = true;
            }
        });

        // This array will contain the final topological sort
        this.topologicalSort = [];
        // Temporary set of nodes to process; initially contains just the root (no incoming edge)
        var nodesToProcess = [ this.root ];
        // Actual Kahn's algorithm
        while (nodesToProcess.length > 0) {
            let currentNode = this.cache[nodesToProcess.pop()];
            let json = currentNode.json;
            
            this.topologicalSort.push(currentNode.name);
            
            for (let d of json.dependencies) {
                let child = this.cache[d];
                delete child.incoming[currentNode.name];
                if (Object.keys(child.incoming).length <= 0) {
                    nodesToProcess.push(child.name);
                }
            }
        }

        // console.log(" Dependencies: ", this.topologicalSort);
        
        // Continue to loading resources
        this.loadResources();
    }

    // Load resources for all requested packages
    loadResources() {
        var loader = new Loader();
        for (let n of this.topologicalSort) {
            let node = this.cache[n];

            if (!node.resources) {
                let json = node.json;
                // Main (constructor) resources
                loader.add(node.name + "%MAIN", this.getBaseDir(node.name) + node.name + "/" + json.main);
                // Other resources
                if (json.resources) {
                    for (let resource of json.resources) {
                        loader.add(node.name + "%" + resource, this.getBaseDir(node.name) + node.name + "/" + resource);
                    }
                }
            }
        }

        loader.onError.add((e) => {
            console.error(e);
            proxy.error("System", "Error loading resource: " + e.message);
        });

        loader.load((loader, resources) => {
            for (let rId of Object.keys(resources)) {
                let resource = resources[rId];
                let parts = rId.split("%");
                let packageName = parts[0];
                let resourceName = parts[1];
                if (resource.error) {
                    proxy.error("System", "Error loading resource " + resourceName + " for package " + packageName + ": " + resource.error);
                }
                let node = this.cache[packageName];
                if (resourceName == "MAIN") {
                    node.main = resource;
                } else {
                    if (!node.resources) {
                        node.resources = {};
                    }
                    node.resources[resourceName] = resource;
                }
            }
            this.instantiate();
        });
        
    }

    // Instantiate all packages (tutorial and its dependencies)
    instantiate() {
        // Instantiate packages in reverse topological order to ensure
        // that the dependencies for every packages are already instantiated
        for (let i = this.topologicalSort.length - 1; i >= 0; i--) {
            let node = this.cache[this.topologicalSort[i]];
            if (!node.instance) {
                this.instantiatePackage(node);
            }
        }
        // Invoke callback (if any) with the instance of the root node
        // a.k.a. the tutorial
        this.finished();
    }

    // Instnatiate a single package
    instantiatePackage(node) {
        proxy.debug("System", " Instantiating package " + node.name);
        // Prepare package constructor function from the loaded 'main' resource
        try {
            node.packageConstructor = eval(node.main.data);
        } catch (e) {
            console.error(e);
            proxy.error("System", "Error evaluating source for package " + node.name + ": " + e);
        }
        // Prepare package dependencies for injection to the constructor
        var dependencies = {};
        for (let d of node.json.dependencies) {
            dependencies[d] = this.cache[d].instance;
            // console.log("injecting D " + d + " to node " + node.name)
        }
        // Instantiate the package by invoking the package constructor
        try {
            node.instance = new node.packageConstructor(dependencies, node.resources);
        } catch (e) {
            console.error(e);
            proxy.error("System", "Error instantiating package " + node.name + ": " + e);
        }
        // Set instance properties; necessary for recursive cleanup;
        node.instance.definition = node.json;
        node.instance.resources = node.resources;
        for (let d of node.json.dependencies) {
            node.instance[d] = this.cache[d].instance;
        }

        // Instantiate package modules
        if (node.json.modules) {
            for (let m of node.json.modules) {
                try {
                    var moduleConstructor = eval(node.resources[m + ".js"].data);
                    node.instance[m] = new moduleConstructor(node.instance);
                } catch (e) {
                    console.error(e);
                    proxy.error("System", "Error instantiating module " + m + " for package " + node.name + ": " + e);
                }
            }
        }
    }

    finished() {
        if (this.callback) {
            this.callback(this.cache[this.root].instance);
        }
    }

}

let packageLoader = new PackageLoader();
export default packageLoader;