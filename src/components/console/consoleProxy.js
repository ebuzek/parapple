import moment from 'moment';

export const LOG_LEVEL = {

    DEBUG: {
        cssClass: "debug-msg"
    },
    INFO: {
        cssClass: "info-msg"
    },
    WARNING: {
        icon: "warning",
        cssClass: "warning-msg"
    },
    ERROR: {
        icon: "warning",
        cssClass: "error-msg"
    },

}

/**
 * Log
 * 
 * Helper class for console log items
 */
class Log {

    constructor(level, key, message) {
        this.level = level;
        this.message = message;
        this.key = key;
        this.time = moment();
        if (typeof(message) == 'object') {
            this.object = true;
        }
    }

}

class ConsoleProxy {

    constructor() {
        this.map = {};
        this._default = null;
    }

    register(_console) {
        if (_console.keys) {
            for (let key of _console.keys) {
                this.map[key] = _console;
            }
        } else {
            this._default = _console;
        }
    }

    log(level, key, message) {
        var log = new Log(level, key, message);
        if (this.map[key]) {
            this.map[key].log(log);
        }
        this._default.log(log);
    }

    debug(key, message) {
        this.log(LOG_LEVEL.DEBUG, key, message);
    }

    info(key, message) {
        this.log(LOG_LEVEL.INFO, key, message);
    }

    warn(key, message) {
        this.log(LOG_LEVEL.WARNING, key, message);
    }

    error(key, message) {
        this.log(LOG_LEVEL.ERROR, key, message);
    } 

}

var consoleProxy = new ConsoleProxy();
export default consoleProxy;

import global from 'utils/global.js';
global.publish(consoleProxy, "console");
