# parapple

> Web platform for parallel programming tutorials

Master thesis project, MFF UK.
Emanuel Buzek (c) 2016-2017

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Documentation
The documentation can found in the following files, or accessed directly from the application (top-right corner in the main menu bar).

- User Manual: userDocs.md
- Developer documentation: devDocs.pdf (thesis appendix A)