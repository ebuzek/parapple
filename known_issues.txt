Current critical/high-priority issues as of 21/July/2017

-Printing circular structure to console breaks the application (or displaying circular objects in debugger)
-Click on buttons/links causes page to scroll up
-Runtime error with single agent causes simulation to SUCCESS
-(missing feature) Ability to HALT agent execution anytime (programatically)