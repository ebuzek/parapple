(function ($element) {

    var data = {
        gridSize: "16",
        channelCount: "8"
    };

    this.getOptions = function() {
        return data;
    }

    this.getData = function() {
        return data;
    }

})