(function (dependencies, resources) {

    var self = this;
       
    var status;

    var gridSize;
    var channelCount;

    this.getConfig = function(options) {
        
        gridSize = parseInt(options.gridSize);
        channelCount = parseInt(options.channelCount);

        self.agents = {  };
        for (var i = 0; i < gridSize; i++) {
            var name = "bug-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeBug(name);
        }

        var agentType = dependencies.ladybugs.agents.typeBug;
        agentType.solution = resources["solutions/bug.js"].data;

        return {
            agentTypes: [ agentType ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {

        PARAPPLE.instructionCost.setConstantProfile();
        
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
    }

    function prepareResults() {
        self.results = [];
        for (var col = 0; col < gridSize; col++) {
            var color = dependencies.ladybugs.pixi2d.grid.getColorCountInColumn(col, dependencies.ladybugs.pixi2d.colors.colors.white) >= gridSize / 2
                ? dependencies.ladybugs.pixi2d.colors.colors.white
                : dependencies.ladybugs.pixi2d.colors.colors.skyblue; 
            self.results.push(color);
            PARAPPLE.console.debug("lockstep", dependencies.ladybugs.pixi2d.colors.codeToName(color));
        }
    }

    function checkResult() {
        var minCol = gridSize - 1;
        for (var agent of Object.values(self.agents)) {
            var coords = agent.getCoords();
            minCol = Math.min(minCol, coords.col);
        }
        
        var done = minCol == gridSize - 1;

        var status = {
            done: done,
            solved: true,
            message: "Great, all columns were colored correctly!"
        };

        for (var col = 0; col < gridSize; col++) {
            var expectedColor = self.results[col];
            var count = dependencies.ladybugs.pixi2d.grid.getColorCountInColumn(col, expectedColor);
            if (count != gridSize) {
                var colorName = dependencies.ladybugs.pixi2d.colors.codeToName(expectedColor);
                status.solved = false;
                status.message = "Oh no. Column with index " + col + " was supposed to be all '" + colorName + "'... Number of " + colorName + " tiles: " + count;
                status.done = true;
                break;
            }
        }

        self.status = status;
        return status;
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();
        dependencies.ladybugs.broadcast.reset(channelCount);
        dependencies.ladybugs.pixi2d.grid.colorGridRandom([
            dependencies.ladybugs.pixi2d.colors.colors.skyblue,
            dependencies.ladybugs.pixi2d.colors.colors.white
        ]);

        prepareResults();

        self.status = {
            done: false,
            solved: false,
            message: ""
        };

        // Position bugs to the left side of the grid
        for (var i = 0; i < gridSize; i++) {
            var bug = self.agents["bug-" + i];
            bug.moveTo(0, i);
            bug.turn(dependencies.ladybugs.pixi2d.gridAgents.orientations.RIGHT);
        }
    }

    this.getStatus = function(finalCheck) {
        if (finalCheck) {
            return checkResult();
        }
        return status;
    }

})