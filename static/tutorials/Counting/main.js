(function (dependencies, resources) {

    console.log("tutorial test", resources, dependencies);

    var self = this;
       
    var correctCount;
    var status;

    this.getConfig = function(options) {
        
        self.agents = {
            
        };

        for (var i = 0; i < options.agentCount; i++) {
            var name = "butterfly-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeButterfly(name);
        }

        var agentTypeButterfly = dependencies.ladybugs.agents.typeButterfly;
        agentTypeButterfly.solution = resources["solutions/butterfly.js"].data;
        

        agentTypeButterfly.submitFunction = function(agent, value) {
			if (value == correctCount) {
                PARAPPLE.console.info(agent.name, "Agent " + agent.name + " correctly submitted value " + value);
				agent.solved = true;
                status.solved = true;
                for (var a of Object.values(self.agents)) {
                    if (!a.solved) {
                        status.solved = false;
                        break;
                    }
                }
				status.done = status.solved;
                return {
                    result: true,
                    cost: PARAPPLE.instructionCost.unit()
                };
            } else {
                status.message = "Agent " + agent.name + " submitted incorrect value " + value + ". Correct value is " + correctCount + ".";
                PARAPPLE.console.info(agent.name, status.message);
                status.done = true;
            }
            return {
               result: false,
               cost: PARAPPLE.instructionCost.unit()
            };
        }

        return {
            agentTypes: [ agentTypeButterfly ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: 10,
            cols: 10
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();
        dependencies.ladybugs.pixi2d.grid.colorGridRandom([
            dependencies.ladybugs.pixi2d.colors.colors.orange,
            dependencies.ladybugs.pixi2d.colors.colors.white
        ]);
		correctCount = dependencies.ladybugs.pixi2d.grid.getColorCount(dependencies.ladybugs.pixi2d.colors.colors.orange);
        status = {
            done: false,
            solved: false,
            message: "All agents have submitted correct value " + correctCount + "."
        };

        // Position agents
        for (var a of Object.values(self.agents)) {
            dependencies.ladybugs.agents.placeRandomly(a);
        }
    }

    this.getStatus = function() {
        return status;
    }

})