for (var row = 0; row < gridRows(); row++) {
    for (var col = 0; col < gridCols(); col++) {
        setColor(COLOR.orange);
        if (row % 2 == 0) {
            east();
        } else {
            west();
        }
    }
    south();
}