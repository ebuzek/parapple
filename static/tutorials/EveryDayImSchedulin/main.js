(function (dependencies, resources) {

    var self = this;
       
    var status;
    var gridSize = 10;

    this.getConfig = function(options) {
        
        self.agents = {  };
        for (var i = 0; i < gridSize; i++) {
            var name = "snail-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeSnail(name);
        }

        var agentTypeSnail = dependencies.ladybugs.agents.typeSnail;
        agentTypeSnail.solution = resources["solutions/snail.js"].data;

        return {
            agentTypes: [ agentTypeSnail ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();
        dependencies.ladybugs.pixi2d.grid.colorGridEven([
            dependencies.ladybugs.pixi2d.colors.colors.white,
            dependencies.ladybugs.pixi2d.colors.colors.lightcyan,
            dependencies.ladybugs.pixi2d.colors.colors.aquamarine,
            dependencies.ladybugs.pixi2d.colors.colors.mediumturquoise,
            dependencies.ladybugs.pixi2d.colors.colors.darkturquoise,
            dependencies.ladybugs.pixi2d.colors.colors.cadetblue
        ]);
        status = {
            done: false,
            solved: false
        };

        // Position snail to the left side of the grid
        for (var i = 0; i < gridSize; i++) {
            var snail = self.agents["snail-" + i];
            snail.moveTo(0, i);
            snail.turn(dependencies.ladybugs.pixi2d.gridAgents.orientations.RIGHT);
        }
    }

    this.getStatus = function() {
        var minCol = gridSize - 1;
        for (var snail of Object.values(self.agents)) {
            var coords = snail.getCoords();
            minCol = Math.min(minCol, coords.col);
        }
        if (minCol == gridSize - 1) {
            status.solved = true;
            status.done = true;
            status.message = "Yesss... all snails made it to the other side.";
            return status;
        }
        return status;
    }

})