function handshake() {
    var ok = false;
    while (!ok) {
        ok = true;
        if (myId() > 0) {
            ok = hasNeighbor(ORIENTATION.TOP);
        }
        if (myId() < 9) {
            ok = ok && hasNeighbor(ORIENTATION.BOTTOM);
        }
    }
    return true;
}

for (var i = 0; i < 10; i++) {
    if (handshake()) {
        forward();   
    }
}