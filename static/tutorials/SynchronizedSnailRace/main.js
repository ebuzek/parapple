(function (dependencies, resources) {

    console.log("tutorial test", resources, dependencies);

    var self = this;
       
    var status;

    var gridSize = 10;

    this.getConfig = function(options) {
        
        self.agents = {  };
        for (var i = 0; i < gridSize; i++) {
            var name = "snail-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeSnail(name);
        }

        var agentTypeSnail = dependencies.ladybugs.agents.typeSnail;
        agentTypeSnail.solution = resources["solutions/snail.js"].data;

        return {
            agentTypes: [ agentTypeSnail ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();
        dependencies.ladybugs.pixi2d.grid.colorGridEven([
            dependencies.ladybugs.pixi2d.colors.colors.skyblue,
            dependencies.ladybugs.pixi2d.colors.colors.powederblue,
            dependencies.ladybugs.pixi2d.colors.colors.lightblue
        ]);
        status = {
            done: false,
            solved: false,
            message: "Awesome, all snails made it to the other side safely!"
        };

        // Position snail to the left side of the grid
        for (var i = 0; i < gridSize; i++) {
            var snail = self.agents["snail-" + i];
            snail.moveTo(0, i);
            snail.turn(dependencies.ladybugs.pixi2d.gridAgents.orientations.RIGHT);
        }
    }

    this.getStatus = function() {
        var minCol = gridSize - 1, maxCol = 0;
        for (var snail of Object.values(self.agents)) {
            var coords = snail.getCoords();
            minCol = Math.min(minCol, coords.col);
            maxCol = Math.max(maxCol, coords.col);
        }
        if (maxCol - minCol > 1) {
            status.message = "<strong>Snail fail!</strong> Your snails did not wait for each other. Look at it, some snails are more than 1 tile apart... Try again ;)",
            status.done = true;
            return status;
        }
        if (minCol == gridSize - 1) {
            status.solved = true;
            status.done = true;
            return status;
        }
        return status;
    }

})