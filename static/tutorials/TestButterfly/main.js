(function (dependencies, resources) {

    console.log("tutorial test", resources, dependencies);

    var self = this;
    
    this.getConfig = function(options) {
        
        self.agents = {
            "butterfly-1": dependencies.ladybugs.agents.makeButterfly("butterfly-1"),
            "butterfly-2": dependencies.ladybugs.agents.makeButterfly("butterfly-2")
        };

        var agentTypeButterfly = dependencies.ladybugs.agents.typeButterfly;
        agentTypeButterfly.solution = resources["solutions/butterfly.js"].data;
        agentTypeButterfly.api.push(dependencies.lakatos.api);

        return {
            agentTypes: [ agentTypeButterfly ],
            agents: Object.values(self.agents)
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: 12,
            cols: 12
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();

        // Position agents
        dependencies.ladybugs.agents.placeRandomly(self.agents["butterfly-1"]);
        dependencies.ladybugs.agents.placeRandomly(self.agents["butterfly-2"]);
    }

    this.getStatus = function() {
        return {
            done: false,
            solved: false,
            message: null
        }
    }

})