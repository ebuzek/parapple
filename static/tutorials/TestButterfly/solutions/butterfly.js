while (true) {

    var x = random();
    if (x < 0.25) {
        north();
        setColor(COLOR.salmon);
    } else if (x < 0.5) {
        west();
        setColor(COLOR.peachpuff);
    } else if ( x < 0.75) {
        south();
        setColor(COLOR.seagreen);
    } else {
        east();
        setColor(COLOR.tomato);
    }

}