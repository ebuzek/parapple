(function ($element) {

    var data = {
        workerCount: "8",
        masterCount: "4",
        gridSize: "12"
    };

    this.getOptions = function() {
        return data;
    }

    this.getData = function() {
        return data;
    }

})