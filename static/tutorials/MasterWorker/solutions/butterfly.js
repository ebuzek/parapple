for (var row = 0; row < gridRows(); row++) {
    for (var col = 0; col < gridCols(); col++) {
        setColor(COLOR.tomato);
        if (row % 2 == 0) {
            east();
        } else {
            west();
        }
    }
    south();
}