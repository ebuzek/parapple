(function (dependencies, resources) {

    var self = this;
    var gridSize;

    this.getConfig = function(options) {
        
        gridSize = parseInt(options.gridSize);
        
        self.agents = {};
        
        for (var i = 0; i < options.workerCount; i++) {
            var name = "worker-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeButterfly(name);
        }
        
        for (var i = 0; i < options.masterCount; i++) {
            var name = "master-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeLadybug(name);
        }

        var agentTypeButterfly = dependencies.ladybugs.agents.typeButterfly;
        agentTypeButterfly.solution = resources["solutions/butterfly.js"].data;
        
        var agentTypeLadybug = dependencies.ladybugs.agents.typeLadybug;
        agentTypeLadybug.solution = resources["solutions/ladybug.js"].data;
        
        return {
            agentTypes: [ agentTypeButterfly, agentTypeLadybug ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        
        PARAPPLE.instructionCost.setAgentProfile();
        
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();

        // Position agents randomly
        dependencies.ladybugs.agents.placeRandomlyAll(self.agents);
    }

    this.getStatus = function() {
        var solved = dependencies.ladybugs.pixi2d.grid.isAllColor(dependencies.ladybugs.pixi2d.colors.colors.tomato);
        return {
            done: solved,
            solved: solved,
            message: "Great, you managed to color the entire grid! Good work ;)"
        }
    }

})