(function (dependencies, resources) {
    
    var self = this;

    this.getConfig = function(options) {

        var worker = {
            name: "Worker",
            api: [
                {
                    name: "Worker",
                    doc: resources["worker.html"]
                },
                dependencies.utils.agents.api,
                dependencies.console.api,
                dependencies.math.api,
                dependencies.utils.api
            ],
            solution: resources["solutions/worker.js"].data
        }

        var agent1 = {
            name: "worker-1",
            agentType: worker
        }

        return {
            agentTypes: [ worker ],
            agents: [ agent1 ],
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function() {
        $("#parapple-canvas").empty();
        $("#parapple-canvas").html(new XMLSerializer().serializeToString(resources["view.html"].data.children[0]));

        dependencies.console.setInterceptor(function(agent, value) {
            if (value && typeof(value) == "string" && value.indexOf(agent.name) > -1) {
                self.status.solved = true;
                self.status.done = true;
                self.status.message = "Nice, agent <strong>" + agent.name + "</strong> just printed its name to the console - you can check for yourself in the console tab.<br/>Good work, and enjoy the coming tutorials!";
            }
        });
    }

    this.reset = function() {
        self.status = {
            done: false,
            solved: false
        }
    }

    this.getStatus = function() {
        return self.status;
    }

})