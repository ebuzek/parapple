for (var i = 0; i < 100; i++) {

    var x = random();
    if (x < 0.25) {
        create(TYPE.apple);
        north();
        forward();
        forward();
    } else if (x < 0.5) {
        create(TYPE.strawberry);
        west();
        forward();
        forward();
        left();
    } else if ( x < 0.75) {
        create(TYPE.flower);
        south();
        forward();
        right();
        forward();
    } else {
        east();
        forward();
    }

}