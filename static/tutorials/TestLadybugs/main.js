(function (dependencies, resources) {

    console.log("tutorial test", resources, dependencies);

    var self = this;
    
    this.getConfig = function(options) {
        
        self.agents = {
            "ladybug-1": dependencies.ladybugs.agents.makeLadybug("ladybug-1"),
            
            "bee-1": dependencies.ladybugs.agents.makeBee("bee-1"),
            "bee-2": dependencies.ladybugs.agents.makeBee("bee-2"),
            
            "beetle-1": dependencies.ladybugs.agents.makeBeetle("beetle-1"),
            "beetle-2": dependencies.ladybugs.agents.makeBeetle("beetle-2"),
            
            "butterfly-1": dependencies.ladybugs.agents.makeButterfly("butterfly-1"),
            "butterfly-2": dependencies.ladybugs.agents.makeButterfly("butterfly-2")
        };

        var agentTypeLadybug = dependencies.ladybugs.agents.typeLadybug;
        agentTypeLadybug.solution = resources["solutions/ladybug.js"].data;

        var agentTypeButterfly = dependencies.ladybugs.agents.typeButterfly;
        agentTypeButterfly.solution = resources["solutions/butterfly.js"].data;

        var agentTypeBeetle = dependencies.ladybugs.agents.typeBeetle;
        agentTypeBeetle.solution = resources["solutions/beetle.js"].data;

        var agentTypeBee = dependencies.ladybugs.agents.typeBee;
        agentTypeBee.solution = resources["solutions/bee.js"].data;
        

        return {
            agentTypes: [ agentTypeLadybug, agentTypeBee, agentTypeBeetle, agentTypeButterfly ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: 10,
            cols: 10
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();
        dependencies.ladybugs.pixi2d.grid.colorGridEven([
            dependencies.ladybugs.pixi2d.colors.colors.white,
            dependencies.ladybugs.pixi2d.colors.colors.salmon,
            dependencies.ladybugs.pixi2d.colors.colors.silver,
            dependencies.ladybugs.pixi2d.colors.colors.tomato
        ]);

        // Position agents
        $.each(self.agents, function(key, agent) {
            dependencies.ladybugs.agents.placeRandomly(agent);
        });
                
        var flower = dependencies.ladybugs.pixi2d.gridObjects.makeObject("flower", "flower");
        flower.show(4,5);
    }

    this.getStatus = function() {
        return {
            done: false,
            solved: false,
            message: null
        }
    }

})