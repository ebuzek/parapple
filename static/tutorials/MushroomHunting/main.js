(function (dependencies, resources) {

    var self = this;
    var gridSize = 12;

    this.getConfig = function(options) {
        
        self.agents = {};
        
        for (var i = 0; i < options.workerCount; i++) {
            var name = "bee-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeBee(name);
        }
        
        var agentType = dependencies.ladybugs.agents.typeBee;
        agentType.solution = resources["solutions/bee.js"].data;
                
        return {
            agentTypes: [ agentType ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();

        dependencies.ladybugs.forrest.makeFromMap(resources["map.json"].data);
        
        // Position agents randomly
        dependencies.ladybugs.agents.placeAllRandomlyInRange(0, 9, 11, 11, self.agents);
    }

    this.getStatus = function() {
        // Tutorial is solved when all apples are harvested
        var solved = dependencies.ladybugs.forrest.harvested == dependencies.ladybugs.objectCount;
        return {
            done: solved,
            solved: solved,
            message: "Great, you managed to color the entire grid! Good work ;)"
        }
    }

})