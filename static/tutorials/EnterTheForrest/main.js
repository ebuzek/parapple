(function (dependencies, resources) {

    var self = this;
    var gridSize = 12;

    var beetleCount = 4;

    this.getConfig = function(options) {
        
        self.agents = {};
        beetleCount = parseInt(options.beetleCount);

        for (var i = 0; i < beetleCount; i++) {
            var name = "beetle-" + i;
            self.agents[name] = dependencies.ladybugs.agents.makeBeetle(name);
        }
        
        var agentType = dependencies.ladybugs.agents.typeBeetle;
        agentType.solution = resources["solutions/beetle.js"].data;
                
        return {
            agentTypes: [ agentType ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: gridSize,
            cols: gridSize
        });
        dependencies.ladybugs.pixi2d.gridAgents.setMovementInterceptor(function(agent, location) {
           if (agent.standsOn("house") && !agent._solved) {
                self.solved++;
                agent._solved = true;
                // TODO: halt() does not work!
                // PARAPPLE.session.haltAgent(agent.name);
                PARAPPLE.console.debug(agent.name, "Awesome, agent " + agent.name + " made it home!");
           }
           if (self.solved == beetleCount) {
              self.status.done = true;
              self.status.solved = true;
              self.status.message = "Congrats, all beetles made it all the way through the forrest to their home ;)";
           }
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();

        dependencies.ladybugs.forrest.makeFromMap(resources["map.json"].data);
        
        // Position agents randomly
        dependencies.ladybugs.agents.placeAllRandomlyInRange(10, 0, 11, 1, self.agents);

        self.solved = 0;
        for (var agent of Object.values(self.agents)) {
            agent._solved = false;
        }

        self.status = {
            done: false,
            solved: false
        }
    }

    this.getStatus = function() {
        return self.status;
    }

})