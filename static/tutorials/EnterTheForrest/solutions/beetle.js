while(true) {
    var color = getColor();
    if (color == COLOR.white) {
        if (random() < 0.5) {
            left();
            setColor(COLOR.salmon);
            forward();
        } else {
            right();
            setColor(COLOR.teal);
            forward();
        }
    } else {
        if (color == COLOR.salmon) {
            left();
            forward();
        } else {
            right();
            forward();
        }
    }
    setColor(COLOR.white);
}