(function (dependencies, resources) {

    var self = this;
    
    this.getConfig = function(options) {
        
        self.agents = {
            "bee-1": dependencies.ladybugs.agents.makeBee("bee-1"),
            "bee-2": dependencies.ladybugs.agents.makeBee("bee-2"),
            "beetle-1": dependencies.ladybugs.agents.makeBeetle("beetle-1"),
            "beetle-2": dependencies.ladybugs.agents.makeBeetle("beetle-2")
        };

        var agentTypeBeetle = dependencies.ladybugs.agents.typeBeetle;
        agentTypeBeetle.solution = resources["solutions/beetle.js"].data;

        var agentTypeBee = dependencies.ladybugs.agents.typeBee;
        agentTypeBee.solution = resources["solutions/bee.js"].data;
        

        return {
            agentTypes: [ agentTypeBee, agentTypeBeetle ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        dependencies.ladybugs.initialize(session, {
            rows: 10,
            cols: 10
        });
    }

    this.reset = function(session) {
        dependencies.ladybugs.reset();

        // Position agents
        dependencies.ladybugs.agents.placeRandomly(self.agents["bee-1"]);
        dependencies.ladybugs.agents.placeRandomly(self.agents["bee-2"]);
        dependencies.ladybugs.agents.placeRandomly(self.agents["beetle-1"]);
        dependencies.ladybugs.agents.placeRandomly(self.agents["beetle-2"]);
    }

    this.getStatus = function() {
        return {
            done: false,
            solved: false,
            message: null
        }
    }

})