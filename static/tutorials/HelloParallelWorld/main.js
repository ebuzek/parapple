(function (dependencies, resources) {
    
    var self = this;
    var agentCount = 10;

    this.getConfig = function(options) {

        var worker = {
            name: "Worker",
            api: [
                {
                    name: "Worker",
                    doc: resources["worker.html"]
                },
                dependencies.utils.agents.api,
                dependencies.console.api,
                dependencies.math.api,
                dependencies.utils.api
            ],
            solution: resources["solutions/worker.js"].data
        }

        self.agents = {};

        for (var i = 0; i < agentCount; i++) {
            var name = "worker-" + i;
            self.agents[name] = {
                name: name,
                agentType: worker
            }
        }
        
        return {
            agentTypes: [ worker ],
            agents: Object.values(self.agents),
            schedulerType: options.schedulerType
        }

    }

    this.initialize = function(session) {
        $("#parapple-canvas").empty();
        $("#parapple-canvas").html(new XMLSerializer().serializeToString(resources["view.html"].data.children[0]));
        $("#parapple-canvas").find("img[data-src='screen_debug.png']").attr('src', resources["screen_debug.png"].data.src);

        // Prepare expected results for all agents
        function factorial(n) {
            var result = 1;
            for (var i = n; i > 0; i--) {
                result *= i;
            }
            return result;
        }
        for (var agent of Object.values(self.agents)) {
            agent._result = factorial(agent.agentId);
        }

         dependencies.console.setInterceptor(function(agent, value) {
            if (value && value == agent._result && !agent._solved) {
                agent._solved = true;
                self.solved++;
            } else {
                self.status.solved = false;
                self.status.done = true;
                self.status.message = "Ooops, agent " + agent.name + " printed incorrect value: " + value;
            }
            if (self.solved == agentCount) {
                self.status.solved = true;
                self.status.done = true;
                self.status.message = "Awesome, it looks like all agents submitted the right result to the console...";
            }
         });
    }

    this.reset = function() {
        self.solved = 0;
        for (var agent of Object.values(self.agents)) {
            agent._solved = false;
        }
        self.status = {
            done: false,
            solved: false,
            message: null
        }
    }

    this.getStatus = function() {
        return self.status;
    }

})