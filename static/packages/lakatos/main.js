(function (dependencies, resources) {

    function lubos(agent, co) {
        var r = resources[co];
        if (r) {
            var data = r.data;
            if (data && data.play) {
                data.play();
                return {
                    result: true,
                    cost: 1
                }
            }
        }
        return {
            result: false,
            cost: 1
        }
    }

    this.api = {
        name: "Lakatoš",
        doc: resources["apiDoc.html"],
        functions: {
            lubos: lubos
        },
        constants: {
            LAKATOS: {
                nebuduToDelat: "nebudu-to-delat.mp3",
                toSouNervy: "to-sou-nervy.mp3"
            }
        }
    }

})