(function (dependencies, resources) {

    var self = this;
    
    self.options = {
        width: 480,
        height: 480
    }

    // Eval pixi package to create PIXI namespace
    eval(resources["pixi.js"].data);

    // Initialize Pixi.js
    var type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
       type = "canvas";
    }
    PIXI.utils.sayHello(type);

    this.initialize = function(options) {

        PARAPPLE.console.debug("pixi", "Initializing package pixi.js...");

        self.options = $.extend(self.options, options);
        // Create the renderer
        self.renderer = PIXI.autoDetectRenderer(self.options.width, self.options.height);
        // Add the canvas to the Simulator element
        $("#parapple-canvas").empty();
        $("#parapple-canvas")[0].append(self.renderer.view);

        // Main pixi container 
        self.stage = new PIXI.Container();
        this.render();
    }

    this.render = function() {
        self.renderer.render(self.stage);
    }

    this.reset = function() {
        
    }

    this.cleanup = function() {
        self.stage.destroy();
        self.renderer.destroy();
        $("#parapple-canvas").empty();
    }
    

})