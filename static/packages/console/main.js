(function (dependencies, resources) {
 
    var self = this;

    this.setInterceptor = function(callback) {
        this.interceptor = callback;
    } 

    function debug(agent, x) {
        if (self.interceptor) {
            self.interceptor(agent, x);
        }
        PARAPPLE.console.debug(agent.name, x);
        return {
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function info(agent, x) {
        if (self.interceptor) {
            self.interceptor(agent, x);
        }
        PARAPPLE.console.info(agent.name, x);
        return {
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function error(agent, x) {
        if (self.interceptor) {
            self.interceptor(agent, x);
        }
        PARAPPLE.console.error(agent.name, x);
        return {
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    this.api = {
        name: "Console",
        doc: resources["apiDoc.html"],
        functions: {
            debug,
            info,
            log: info,
            print: info,
            error
        }
    }

})