(function (dependencies, resources) {

    var MODE = {
        WRITE: 0,
        READ: 1
    }

    var channelCount = 0;
    var channels;

    var debug = false;

    function broadcast(agent, mode, channel, value) {
        var result;
        if (mode == MODE.WRITE) {
            // WRTIE
            if (channel < 0 || channel >= channelCount) {
                if (debug) PARAPPLE.console.warn(agent.name, "Agent broadcast read channel number out of bounds: " + channel);
                result = false;
            } else {
                if (channels[channel]) {
                    // This channel is already in use; break the message
                    channels[channel] = undefined;
                    if (debug) PARAPPLE.console.warn(agent.name, "Repeated broadcast on channel " + channel + "; message distorted.");
                    result = false;
                } else {
                    // Channel free; write
                    channels[channel] = value;
                    if (debug) PARAPPLE.console.debug(agent.name, "Write value " + value + " to channel " + channel);
                    result = true;
                }
            }
        } else {
            // READ
            function createPromise(_channel, _agent) {
                return function() {
                    var _result = channels[_channel];
                    if (debug) PARAPPLE.console.debug(_agent.name, "Value read from channel " + _channel + ": " + _result);
                    return _result;
                }
            }
            result = createPromise(channel, agent);
        }
        return {
            result: result,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function enableBroadcastDebug(agent) {
        debug = true;
        return {
            result: true,
            cost: PARAPPLE.instructionCost.free()
        }
    }

    function getChannelCount(agent) {
        return {
            result: channelCount,
            cost: PARAPPLE.instructionCost.free()
        }
    }


    function lockstepCallback(last, activeExecutors) {
        if (last.id == 'CALL_PACKAGE' && last.params.name == 'broadcast') {
            // Redeem promises: active executors from last tick might have called
            // broadcast(READ) - they received a promise function which now must
            // be evaluated so that they have the actual READ result on the stack
            for (var e of activeExecutors) {
                e.redeemPromise();
            }
        }
        // Destroy broadcast channels
        channels = [];
    }

    this.initialize = function(session) {
        session.lockstepCallbacks["broadcast"] = lockstepCallback;
    }

    this.cleanup = function() {
        delete this.session.lockstepCallbacks["broadcast"];
    }

    this.reset = function(count) {
        PARAPPLE.console.debug("broadcast", "Initialized lock-step broadcast channels: " + count);
        debug = false;
        channelCount = count;
        channels = [];
    }

    this.api = {
        name: "Broadcast",
        doc: resources["apiDoc.html"],
        functions: {
            broadcast: broadcast,
            channelCount: getChannelCount,
            enableBroadcastDebug: enableBroadcastDebug
        },
        constants: {
            MODE: MODE
        }
    }

})