(function (utils) {

    function myId(agent) {
        return {
            result: agent.agentId,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function myName(agent) {
        return {
            result: agent.name,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function myType(agent) {
        return {
            result: agent.agentType.name,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function agentNames(agent) {
        return {
            result: PARAPPLE.session.agentNames,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function agentTypes(agent) {
        return {
            result: PARAPPLE.session.agentTypeNames,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function agentCount(agent) {
        return {
            result: PARAPPLE.session.agentNames.length,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function submit(agent, value) {
        if (agent.agentType.submitFunction) {
            return agent.agentType.submitFunction(agent, value);
        } else {
            PARAPPLE.console.error("Submit function is not specified for this agent type or tutorial.");
            return {
                cost: PARAPPLE.instructionCost.free()
            }
        }
    } 

    this.api = {
        name: "Agents",
        doc: utils.resources["agents.html"],
        functions: {
            myId: myId,
            myName: myName,
            myType: myType,
            agentNames: agentNames,
            agentTypes: agentTypes,
            agentCount: agentCount,
            submit: submit
        },
        constants: {
        }
    }


})