(function (dependencies, resources) {

    function length(agent, x) {
        return {
            result: x.length,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function length(agent, x) {
        return {
            result: x.length,
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function mapKeys(agent, x) {
        return {
            result: Object.keys(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function mapValues(agent, x) {
        return {
            result: Object.values(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function indexOf(agent, array, el) {
        return {
            result: array.indexOf(el),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function typeOf(agent, x) {
        var result;
        if (x === undefined || x === null) {
            result = "null";
        } else {
            result = typeof(x); // Works fine Number, String
            if (result == "object") {
                if (x.constructor.name == "Array") {
                    result = "array";
                } else if (x.constructor.name == "Object") {
                    result = "map";
                } else {
                    PARAPPLE.console.warn("Unknown data type: " + x);
                    result = "unknown";
                }
            }
        }
        
        return {
            result: result,
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function toNumber(agent, x) {
        return {
            result: parseFloat(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function toBoolean(agent, x) {
        return {
            result: Boolean(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function toString(agent, x) {
        return {
            result: "" + x,
            cost: PARAPPLE.instructionCost.unit()
        }   
    } 

    this.api = {
        name: "Utils",
        doc: resources["utils.html"],
        functions: {
            length: length,
            mapKeys: mapKeys,
            mapValues: mapValues,
            indexOf: indexOf,
            typeof: typeOf,
            toNumber: toNumber,
            toBoolean: toBoolean,
            toString: toString
        },
        constants: {
        }
    }

})