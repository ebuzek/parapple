(function (pixi2d) {

    this.location = {
        name: "Location",
        doc: pixi2d.resources["apidocs/location.html"],
        functions: {
            location: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.getCoords()
                }
            },
            orientation: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.orientation
                }
            },
            gridRows: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: pixi2d.grid.options.rows
                }
            },
            gridCols: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: pixi2d.grid.options.cols
                }
            }
        },
        constants: {
            ORIENTATION: pixi2d.gridAgents.orientations
        }
    }

    this.walking = {
        name: "Walking",
        doc: pixi2d.resources["apidocs/walking.html"],
        functions: {
            forward: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.forward()
                }
            },
            right: function(agent) {
                agent.turnClockwise();
                return {
                    cost: PARAPPLE.instructionCost.unit(agent)
                }
            },
            left: function(agent) {
                agent.turnCounterclockwise();
                return {
                    cost: PARAPPLE.instructionCost.unit(agent)
                }
            }
        }
    }

    this.messages = {
        name: "Messages",
        doc: pixi2d.resources["apidocs/messages.html"],
        functions: {
            write: function(agent, message) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.dropMessage(message)
                }
            },
            read: function(agent, orientation) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.pickupMessage(orientation)
                }
            },
            hasNeighbor: function(agent, orientation) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.standsNextToAgent(orientation)
                }
            }
        }
    }

    this.flying = {
        name: "Flying",
        doc: pixi2d.resources["apidocs/flying.html"],
        functions: {
            north: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.north()
                }
            },
            south: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.south()
                }
            },
            east: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.east()
                }
            },
            west: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.west()
                }
            }
        }
    }

    this.coloring = {
        name: "Coloring",
        doc: pixi2d.resources["apidocs/coloring.html"],
        functions: {
            getColor: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.getColor()
                }
            },
            setColor: function(agent, color) {
                agent.setColor(color);
                return {
                    cost: PARAPPLE.instructionCost.unit(agent)
                }
            }
        },
        constants: {
            COLOR: pixi2d.colors.colors
        }
    }

    this.masterColoring = {
        name: "Master Coloring",
        doc: pixi2d.resources["apidocs/masterColoring.html"],
        functions: {
            getColor: function(agent, col, row) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: pixi2d.grid.getTileColor(col, row)
                }
            }
        },
        constants: {
            COLOR: pixi2d.colors.colors
        }
    }

    this.objects = {
        name: "Objects",
        doc: pixi2d.resources["apidocs/objects.html"],
        functions: {
            create: function(agent, type) {
                return {
                    cost: PARAPPLE.instructionCost.free(),
                    result: agent.createObject(type).key
                }
            },
            pickup: function(agent, type) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.pickupFirst(type)
                }
            },
            drop: function(agent, type) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.dropFirst(type)
                }
            },
            hasMore: function(agent, type) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.hasType(type)
                }
            },
            standsOn: function(agent, type) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.standsOn(type)
                }
            }
        },
        constants: {
            TYPE: {
                apple: "apple",
                flower: "flower",
                strawberry: "strawberry"
            }
        }
    }

})