(function (dependencies, resources) {

    var self = this;

    var defaultOptions = {

    }

    this.initialize = function(session, options) {
        self.pixi.initialize();
        self.grid.initialize(options);
        this.gridAgents.initialize();
    }

    this.reset = function() {
        // Remove all objects
        self.grid.reset();
        self.gridObjects.cleanup();
        self.utils.reset();
        self.pixi.render();
    }
    
    this.cleanup = function() {
        PARAPPLE.console.debug("pixi2d", "Pixi2d resources cleanup...");
        self.gridObjects.cleanup();
    }

})