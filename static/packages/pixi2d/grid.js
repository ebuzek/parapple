(function (pixi2d) {

    var self = this;
    
    // Default options (can be overridden in initialize)
    self.options = {
            rows: 10,
            cols: 10,
            lineColor: 0xC0C0C0,
            tileColor: 0xFFFFFF,
            tilePadding: 3 // grid object padding for images in px
        };

    // Aggregated counts of tiles of each color for faster/simpler access
    // Note: tile color must be set using Tile.setColor in order for this
    // to work properly...
    var colorCounts = {};

    function Tile(col, row) {
        this.col = col;
        this.row = row;
        this.x = col * self.options.tileWidth;
        this.y = row * self.options.tileHeight;
        this.width = self.options.tileWidth;
        this.height = self.options.tileHeight;
        this.centerX = this.x + this.width / 2;
        this.centerY = this.y + this.height / 2;
        this.lineColor = self.options.lineColor;
        this.color = self.options.tileColor;
		if (this.color !== undefined)
			colorCounts[this.color] = self.getColorCount(this.color) + 1;

		this.rectangle = pixi2d.shapes.rectangle(this);

        // Maps key -> gridObject/gridAgent
        this.objects = {};
        this.agents = {};
    }

    Tile.prototype.setColor = function(color, doNotRender) {
        if (color != this.color) {
            if (this.color !== undefined && colorCounts[this.color]) {
                colorCounts[this.color] = self.getColorCount(this.color) - 1;
            }
            colorCounts[color] = self.getColorCount(color) + 1;
		}
        this.color = color;
        this.rectangle = pixi2d.shapes.rectangle(this);
        if (!doNotRender) {
            pixi2d.pixi.render();
        }
        return this;
    }

    Tile.prototype.getColor = function(color) {
        return this.color;
    }

    Tile.prototype.setColorByName = function(colorName) {
        this.setColor(pixi2d.colors.nameToCode(colorName));
        return this;
    }

    Tile.prototype.getColorByName = function(color) {
        return pixi2d.colors.codeToName(this.color);
    }

    // Get grid tile by relative distance from this tile
    Tile.prototype.getNeighbor = function(dCol, dRow) {
        return self.getTile(this.col + dCol, this.row + dRow);
    }

    var tiles = [];
    this.getIndex = function(col, row) {
        return row * self.options.cols + col;
    }
    this.getTile = function(col, row) {
        if (col < 0 || col >= this.options.cols || row < 0 || row >= this.options.rows) {
            return null;
        } 
        return tiles[this.getIndex(col, row)];
    }
    this.getTiles = function() {
        return tiles;
    }
    this.getTileCount = function() {
        return tiles.length;
    }
    this.setTile = function(col, row, tile) {
		tiles[this.getIndex(col, row)] = tile;
    }
    // Retrieve tiles from given range as a list
    this.getRange = function(c1, r1, c2, r2) {
        var result = [];
        for (var row = r1; row <= r2; row++) {
            for (var col = c1; col <= c2; col++) {
                result.push(this.getTile(col, row));
            }
        }
        return result;
    }

    this.initialize = function(options) {
        self.options = $.extend(self.options, options);
        self.options.tileWidth = pixi2d.pixi.options.width / self.options.cols;
        self.options.tileHeight = pixi2d.pixi.options.height / self.options.rows;
        self.options.objectWidth = self.options.tileWidth - self.options.tilePadding * 2;
        self.options.objectHeight = self.options.tileHeight - self.options.tilePadding * 2;
        for (var r = 0; r < self.options.rows; r++) {
            for (var c = 0; c < self.options.cols; c++) {
                self.setTile(c, r, new Tile(c, r));        
            }
		}
    }

    this.reset = function() {
        colorCounts = {};		
        for (var tile of tiles) {
            tile.message = null;
        }
    }

    this.getTileColor = function(col, row) {
        var tile = this.getTile(col, row);
        if (tile) {
            return tile.getColor();
        }
        return null;
    }

    this.colorGrid = function(color) {
        this._colorGrid(function() { return color });
    }

    this.colorGridEven = function(colors) {
        this._colorGrid(function(c, r) { return colors[self.getIndex(c, r) % colors.length] });
    }

    this.colorGridRandom = function(colors) {
        this._colorGrid(function(c, r) { return colors[Math.floor(Math.random() * colors.length)] });
    }

    this._colorGrid = function(colorFunction) {
        for (var r = 0; r < self.options.rows; r++) {
            for (var c = 0; c < self.options.cols; c++) {
                this.getTile(c, r).setColor(colorFunction(c, r), true);
            }
        }
        pixi2d.pixi.render();
    }

    this.getColorCount = function(color) {
        return colorCounts[color] ? colorCounts[color] : 0;
    }

    this.getColorCountInColumn = function(col, color) {
        var result = 0;
        for (var row = 0; row < self.options.rows; row++) {
            if (self.getTileColor(col, row) == color) {
                result++;
            }
        }
        return result;
    }

    this.getColorCountInRow = function(row, color) {
        var result = 0;
        for (var col = 0; col < self.options.cols; col++) {
            if (self.getTileColor(col, row) == color) {
                result++;
            }
        }
        return result;
    }

    this.isAllColor = function(color) {
        return this.getColorCount(color) == this.getTileCount();
    }

})