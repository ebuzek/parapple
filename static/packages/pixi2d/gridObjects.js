(function (pixi2d) {

    var self = this;
    
    function GridObject(key, image, blocking) {
        this.key = key;
        this.image = image;
        this.type = image.textureKey;
        // Agents cannot enter tiles with blocking objects
        // Can be used for walls etc...
        this.blocking = blocking;
    }

    var uniqueId = 0;

    GridObject.prototype.show = function(col, row) {
        this.tile = pixi2d.grid.getTile(col, row);
        this.image.show(this.tile.centerX, this.tile.centerY);
        this.tile.objects[this.key] = this;
        if (this.blocking) {
            this.tile.blocked = true;
        }
    }

    GridObject.prototype.hide = function(col, row) {
        this.image.hide();
        delete this.tile.objects[this.key];
        this.tile = null;
        if (this.blocking) {
            this.tile.blocked = false;
        }
    }

    // Texture cache; maps resource name (key) -> GridObject
    var cache = {};

    this.makeObject = function(key, textureKey, blocking) {
        if (key === true) {
            key = textureKey + uniqueId;
        }
        var image = pixi2d.images.makeImage(textureKey, {
            width: pixi2d.grid.options.objectWidth,
            height: pixi2d.grid.options.objectHeight
        })
        var gridObject = new GridObject(key, image, blocking);
        if (key) {
            cache[key] = gridObject;
        }
        uniqueId++;
        return gridObject;
    }

    this.get = function(key) {
        return cache[key];
    }
    
    /**
     * @todo Cleanup objects
     * 
     * Destroy all sprites of all objects
     */
    this.cleanup = function() {
        $.each(cache, function(key, gridObject) {
            gridObject.image.sprite.destroy();
        });
        cache = {};
        uniqueId = 0;
    }

})