(function (pixi2d) {

    var self = this;
    
    this.rectangle = function(options) {
       
        var rectangle = options.rectangle;
        if (!rectangle) {
            rectangle = new PIXI.Graphics();
            pixi2d.pixi.stage.addChild(rectangle);
        } else {
            rectangle.clear();
        }

        options = $.extend({
            lineWidth: 1,
            lineStyle: 1,
            lineColor: 0xFFFFFF,
            x: 0,
            y: 0,
            width: 24,
            height: 24,
            color: 0xFFFFFF
        }, options);

        rectangle.lineStyle(options.lineWidth, options.lineColor, options.lineStyle);
        rectangle.beginFill(options.color);
        rectangle.drawRect(0, 0, options.width, options.height);
        rectangle.endFill();
        rectangle.x = options.x;
        rectangle.y = options.y;

        return rectangle;
    }
    

})