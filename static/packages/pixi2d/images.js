(function (pixi2d) {

    var self = this;
    
    // Texture cache; maps resource name (key) -> BaseTexture
    var cache = {};

    this.makeTexture = function(resource, textureKey, rectangle) {
        if (cache[textureKey]) {
            return cache[textureKey];
        }
        var texture = new PIXI.Texture(new PIXI.BaseTexture(resource.data));
        cache[textureKey] = texture;
        if (rectangle) {
            texture.frame = rectangle;
        }
        return texture;
    }

    function Image(textureKey, options) {
        this.textureKey = textureKey;
        
        var texture = cache[textureKey];
        if (!texture) {
            throw "No texture! Key: " + textureKey;
        }

        this.sprite = new PIXI.Sprite(texture);
        $.extend(this.sprite, options);
        this.sprite.visible = false;
        // Rotation anchor in the centre
        this.sprite.anchor.x = 0.5;
        this.sprite.anchor.y = 0.5;
        this.sprite.rotation = 0;
        // Add sprite to the main pixi container
        pixi2d.pixi.stage.addChild(this.sprite);
    }

    Image.prototype.hide = function() {
        this.sprite.visible = false;
        pixi2d.pixi.render();
    }

    Image.prototype.show = function(x, y) {
        this.sprite.x = x;
        this.sprite.y = y;
        this.sprite.visible = true;
        pixi2d.pixi.render();
    }

    Image.prototype.setRotation = function(angle) {
        this.sprite.rotation = angle;
        pixi2d.pixi.render();
    }

    Image.prototype.rotate = function(angle) {
        this.sprite.rotation += angle;
        pixi2d.pixi.render();
    }

    Image.prototype.rotateRight = function() {
        this.rotate(Math.PI / 2);
    }

    Image.prototype.rotateLeft = function() {
        this.rotate(-Math.PI / 2);
    }

    this.makeImage = function(textureKey, options) {
        return new Image(textureKey, options);
    }

    /**
     * @todo Cleanup textures
     */

})