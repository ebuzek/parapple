(function (pixi2d) {

    var self = this;
    
    var orientations = {
        TOP: 0,
        LEFT: -Math.PI/2,
        BOTTOM: Math.PI,
        RIGHT: Math.PI/2
    }
    orientations.NORTH = orientations.TOP;
    orientations.SOUTH = orientations.BOTTOM;
    orientations.EAST = orientations.RIGHT;
    orientations.WEST = orientations.LEFT;

    this.orientations = orientations;

    // Message passing vizualization
    pixi2d.images.makeTexture(pixi2d.resources["images/message.png"], "message");

    // Callback function to invoke after agent move
    // Useful for tracking agent positions for tutorial state
    this.setMovementInterceptor = function(callback) {
        this.movementCallback = callback;
    }

    function getDelta(orientation) {
        var dCol = 0;
        var dRow = 0;
        switch(orientation) {
            case orientations.TOP:
                dRow = -1;
                break;
            case orientations.LEFT:
                dCol = -1;
                break;
            case orientations.BOTTOM:
                dRow = 1;
                break;
            case orientations.RIGHT:
                dCol = 1;
                break;
        }
        return { dCol, dRow };
    }

    function GridAgent(name, textureKey, blocking, capacity) {
        this.name = name;
        this.textureKey = textureKey;
        this.orientation = orientations.TOP;
        this.blocking = blocking;
        this.capacity = capacity || false; // Max object number of objects to carry; false: no limit

        // Map of picked up gridObjects
        this.gridObjects = {};
    }

    GridAgent.prototype.initialize = function() {
        this.gridObject = pixi2d.gridObjects.makeObject(false, this.textureKey, this.blocking);
    }

    GridAgent.prototype.getCoords = function() {
        return {
            col: this.gridObject.tile.col,
            row: this.gridObject.tile.row
        }
    }

    GridAgent.prototype.moveTo = function(col, row) {
        if (col < 0 || col > pixi2d.grid.options.cols - 1 || row < 0 || row > pixi2d.grid.options.rows - 1) {
            // Fail: out of grid bounds
            return false;
        }
        var newTile = pixi2d.grid.getTile(col, row);
        if (newTile.blocked) {
            // New position is blocked (typically by a blocking object or other (blocking) agent...)
            return false;
        }
        // Remove agent from original tile
        if (this.gridObject.tile) {
            delete this.gridObject.tile.agents[this.name];
        }
        // Show agent's grid object at new position
        this.gridObject.show(col, row);
        // Add agent to new tile
        if (this.gridObject) {
            this.gridObject.tile.agents[this.name] = this;
        }
        // Call movement interceptor if applicable
        if (self.movementCallback) {
            self.movementCallback(this, { col: col, row: row });
        }
        // Successful move
        return true;
    }

    GridAgent.prototype.moveBy = function(deltaCol, deltaRow) {
        var coords = this.getCoords();
        return this.moveTo(coords.col + deltaCol, coords.row + deltaRow);
    }

    GridAgent.prototype.turn = function(orientation) {
        this.orientation = orientation;
        this.gridObject.image.setRotation(orientation);
        return true;
    }

    GridAgent.prototype.turnCounterclockwise = function() {
        switch(this.orientation) {
            case orientations.TOP:
                this.orientation = orientations.LEFT;
                break;
            case orientations.LEFT:
                this.orientation = orientations.BOTTOM;
                break;
            case orientations.BOTTOM:
                this.orientation = orientations.RIGHT;
                break;
            case orientations.RIGHT:
                this.orientation = orientations.TOP;
                break;
        }
        this.gridObject.image.setRotation(this.orientation);
        return true;
    }

    GridAgent.prototype.turnClockwise = function() {
        switch(this.orientation) {
            case orientations.TOP:
                this.orientation = orientations.RIGHT;
                break;
            case orientations.LEFT:
                this.orientation = orientations.TOP;
                break;
            case orientations.BOTTOM:
                this.orientation = orientations.LEFT;
                break;
            case orientations.RIGHT:
                this.orientation = orientations.BOTTOM;
                break;
        }
        this.gridObject.image.setRotation(this.orientation);
        return true;
    }

    GridAgent.prototype.getNeighboringTile = function(orientation) {
        var delta = getDelta(orientation);
        return this.gridObject.tile.getNeighbor(delta.dCol, delta.dRow);
    }

    GridAgent.prototype.forward = function() {
        var delta = getDelta(this.orientation);
        return this.moveBy(delta.dCol, delta.dRow);
    }

    GridAgent.prototype.north = function() {
        return this.moveBy(0, -1);
    }
    
    GridAgent.prototype.south = function() {
        return this.moveBy(0, 1);
    }

    GridAgent.prototype.east = function() {
        return this.moveBy(1, 0);
    }

    GridAgent.prototype.west = function() {
        return this.moveBy(-1, 0);
    }

    GridAgent.prototype.createObject = function(textureKey) {
        this.type = textureKey;
        var o = pixi2d.gridObjects.makeObject(true, textureKey);
        var coords = this.getCoords();
        o.show(coords.col, coords.row);
        return o;
    }

    GridAgent.prototype.pickup = function(key) {
        if (this.capacity && Object.keys(this.gridObjects).length >= this.capacity) {
            PARAPPLE.console.debug(this.name, "Agent is already carrying " + this.capacity + " objects, so it cannot pickup another " + key + ".");
            return false;
        } 
        var o = this.gridObject.tile.objects[key];
        if (o) {
            PARAPPLE.console.debug(this.name, "Agent picked up " + key + ".");
            o.hide();
            this.gridObjects[key] = o;
        }
        return o;
    }

    GridAgent.prototype.drop = function(key) {
        var o = this.gridObjects[key];
        if (o) {
            PARAPPLE.console.debug(this.name, "Agent dropped " + key + ".");
            var coords = this.getCoords();
            o.show(coords.col, coords.row);
            delete this.gridObjects[key];
        }
        return o;
    }

    GridAgent.prototype.dropFirst = function(type) {
        var o = false;
        $.each(this.gridObjects, function(key, object) {
            if (object.type == type) {
                o = object;
            }
        });
        if (o) {
            return this.drop(o.key);
        }
    }

    GridAgent.prototype.pickupFirst = function(type) {
        var o = false;
        $.each(this.gridObject.tile.objects, function(key, object) {
            if (object.type == type) {
                o = object;
            }
        });
        if (o) {
            return this.pickup(o.key);
        }
    }

    GridAgent.prototype.destroyFirst = function(type) {
        var o = false;
        $.each(this.gridObject.tile.objects, function(key, object) {
            if (object.type == type) {
                o = object;
            }
        });
        if (o) {
            return o.hide();
        }
    }

    GridAgent.prototype.standsOn = function(type) {
        var result = false;
        $.each(this.gridObject.tile.objects, function(key, object) {
            if (object.type == type) {
                result = true;
            }
        });
        return result;
    }

    GridAgent.prototype.hasType = function(type) {
        var result = false;
        $.each(this.gridObjects, function(key, object) {
            if (object.type == type) {
                result = true;
            }
        });
        return result;
    }

    GridAgent.prototype.setColor = function(color) {
        this.gridObject.tile.setColor(color);
    }

    GridAgent.prototype.getColor = function() {
        return this.gridObject.tile.getColor();
    }

    GridAgent.prototype.hasMessage = function(orientation) {
        var tile = this.getNeighboringTile(orientation);
        return tile && tile.message;
    }

    GridAgent.prototype.dropMessage = function(message) {
        if (!this.hasMessage()) {
            this.createObject("message");
        }
        this.gridObject.tile.message = message;
    }

    GridAgent.prototype.pickupMessage = function(orientation) {
        var tile = this.getNeighboringTile(orientation);
        if (tile && tile.message) {
            this.destroyFirst("message");
            var message = tile.message;
            tile.message = null;
            return message;
        }
        return null;
    }

    GridAgent.prototype.standsNextToAgent = function(orientation) {
        var tile = this.getNeighboringTile(orientation);
        if (tile) {
            return Object.keys(tile.agents).length > 0;
        } else {
            return false;
        }
    }

    // Texture cache; maps resource name (name) -> GridAgent
    var cache = {};

    this.makeAgent = function(name, textureKey) {
        var agent = new GridAgent(name, textureKey);
        cache[name] = agent;
        return agent;
    }
    
    this.get = function(name) {
        return cache[name];
    }

    this.initialize = function() {
        this.movementCallback = null;
        $.each(cache, function(name, agent) {
            agent.initialize();
        });
    }

    /**
     * @todo Cleanup agents
     */
    this.cleanup = function() {
        this.movementCallback = null;
    }

})