(function (pixi2d) {

/**
 * Shuffle array in place
 * Adopted from http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
 */
this.shuffle = function(a) {
    var j, x;
    for (var i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a;
}

/**
 * Shuffles an array into a new array of the same length.
 */
this.shuffleCopy = function(original) {
    var a = [];
    for (var i = 0; i < original.length; i++) {
        a[i] = original[i];
    }
    return this.shuffle(a);
 }

 this.reset = function() {
     this.randomizedTiles = this.shuffleCopy(pixi2d.grid.getTiles());
 }

 this.nextRandomTile = function() {
     return this.randomizedTiles.pop();
 }

})