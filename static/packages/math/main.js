(function (dependencies, resources) {

    function sin(agent, x) {
        return {
            result: Math.sin(x),
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function cos(agent, x) {
        return {
            result: Math.cos(x),
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    function tan(agent, x) {
        return {
            result: Math.tan(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function sqrt(agent, x) {
        return {
            result: Math.sqrt(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function log(agent, x) {
        return {
            result: Math.log(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function log10(agent, x) {
        return {
            result: Math.log10(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function log2(agent, x) {
        return {
            result: Math.log2(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function round(agent, x) {
        return {
            result: Math.round(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function floor(agent, x) {
        return {
            result: Math.floor(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function ceil(agent, x) {
        return {
            result: Math.ceil(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function abs(agent, x) {
        return {
            result: Math.abs(x),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function min(agent, x, y) {
        return {
            result: Math.min(x, y),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function max(agent, x, y) {
        return {
            result: Math.max(x, y),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function pow(agent, x, y) {
        return {
            result: Math.pow(x, y),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function random(agent) {
        return {
            result: Math.random(),
            cost: PARAPPLE.instructionCost.unit()
        }    
    }

    function isNaN(agent, x) {
        return {
            result: Number.isNaN(x),
            cost: PARAPPLE.instructionCost.unit()
        }
    }

    this.api = {
        name: "Math",
        doc: resources["apiDoc.html"],
        functions: {
            sin,
            cos,
            tan,
            log,
            log2,
            log10,
            sqrt,
            round,
            floor,
            ceil,
            abs,
            min,
            max,
            pow,
            random,
            isNaN
        },
        constants: {
            PI: Math.PI,
            E: Math.E,
            Number: {
                NaN: NaN,
                MIN_VALUE: Number.MIN_VALUE,
                MAX_VALUE: Number.MAX_VALUE,
                NEGATIVE_INFINITY: Number.NEGATIVE_INFINITY,
                POSITIVE_INFINITY: Number.POSITIVE_INFINITY
            }
        }
    }

})