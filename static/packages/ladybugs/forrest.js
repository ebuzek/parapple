(function (ladybugs) {

    var mainObject = "mushroom";

    var self = this;
    ladybugs.pixi2d.images.makeTexture(ladybugs.resources["images/tree.png"], "tree");
    ladybugs.pixi2d.images.makeTexture(ladybugs.resources["images/house.png"], "house");

    this.reset = function() {
        this.harvested = 0;
        this.objectCount = 0;
    }

    this.setForrest = function(col, row) {
        var tree = ladybugs.pixi2d.gridObjects.makeObject(true, "tree", true); // uniqueId=true, blocking=true
        tree.show(col, row);
    }

    this.setObject = function(col, row) {
        var obj = ladybugs.pixi2d.gridObjects.makeObject(true, mainObject); // uniqueId=true, blocking=false
        obj.show(col, row);
        this.objectCount++;
    }

    this.setHouse = function(col, row) {
        var obj = ladybugs.pixi2d.gridObjects.makeObject(true, "house"); // uniqueId=true, blocking=false
        obj.show(col, row);
    }

    this.makeFromMap = function(map) {
        var gridOptions = ladybugs.pixi2d.grid.options;
        if (gridOptions.rows != map.length) {
            PARAPPLE.console.error("ladybugs", "Forrest map must contain the same number of rows as pixi2d grid. Map row count: " + map.length);
            return;
        }
        for (var row = 0; row < gridOptions.rows; row++) {
            if (gridOptions.rows != map[row].length) {
                PARAPPLE.console.error("ladybugs", "Each row of forrest map must have the same length as the number of columns in pixi2d grid. Row " + row + " has length " + map[row].length);
                return;
            }
            for (var col = 0; col < gridOptions.cols; col++) {
                var data = map[row][col];
                if (data == 1) {
                    this.setForrest(col, row);
                } else if (data == 2) {
                    this.setObject(col, row);
                } else if (data == 3) {
                    this.setHouse(col, row);
                }
            }
        }
    }

    function drop(agent) {
        var apple = agent.dropFirst(mainObject);
        if (apple && agent.standsOn("house")) {
            apple.hide();
            self.harvested++;
            PARAPPLE.console.debug(agent.name, "Successfully dropped " + mainObject + " in the house. Harvested total: " + self.harvested);
        }
    }
    
    this.api = {
        name: "Forrest",
        doc: ladybugs.resources["apidocs/forrest.html"],
        functions: {
            pickup: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.pickupFirst(mainObject)
                }
            },
            drop: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: drop(agent)
                }
            },
            hasMore: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.hasType(mainObject)
                }
            },
            standsOn: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.standsOn(mainObject)
                }
            }
        },
        constants: {
        }
    }

})