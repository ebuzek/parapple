(function (dependencies, resources) {

    var self = this;

    // Prepare textures
    // (icons from http://www.flaticon.com/packs/animals-19/2)
    dependencies.pixi2d.images.makeTexture(resources["images/ladybug.png"], "ladybug");
    dependencies.pixi2d.images.makeTexture(resources["images/flower.png"], "flower");
    dependencies.pixi2d.images.makeTexture(resources["images/snail.png"], "snail");
    dependencies.pixi2d.images.makeTexture(resources["images/strawberry.png"], "strawberry");
    dependencies.pixi2d.images.makeTexture(resources["images/cat.png"], "cat");
    dependencies.pixi2d.images.makeTexture(resources["images/apple.png"], "apple");
    dependencies.pixi2d.images.makeTexture(resources["images/mushroom.png"], "mushroom");
    dependencies.pixi2d.images.makeTexture(resources["images/spider.png"], "spider");
    dependencies.pixi2d.images.makeTexture(resources["images/beetle.png"], "beetle");
    dependencies.pixi2d.images.makeTexture(resources["images/bee.png"], "bee");
    dependencies.pixi2d.images.makeTexture(resources["images/butterfly.png"], "butterfly");
    dependencies.pixi2d.images.makeTexture(resources["images/fly.png"], "fly");
    dependencies.pixi2d.images.makeTexture(resources["images/bug.png"], "bug");
    
    this.initialize = function(session, options) {
        dependencies.pixi2d.initialize(session, options);
        dependencies.broadcast.initialize(session);
    }

    this.reset = function() {
        dependencies.pixi2d.reset();
        dependencies.pixi2d.grid.colorGrid(dependencies.pixi2d.colors.colors.white);
        this.forrest.reset();
    }

    this.cleanup = function() {
    }

})