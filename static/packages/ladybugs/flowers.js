(function (ladybugs) {

    this.flowersMoving = {
        name: "Flowers (moving)",
        doc: ladybugs.resources["apidocs/flowersMoving.html"],
        functions: {
            pickup: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.pickupFirst("flower")
                }
            },
            drop: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.dropFirst("flower")
                }
            },
            hasMore: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.hasType("flower")
                }
            },
            standsOn: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.standsOn("flower")
                }
            }
        },
        constants: {
        }
    }

    this.flowersMaking = {
        name: "Flowers (making)",
        doc: ladybugs.pixi2d.resources["apidocs/flowersMaking.html"],
        functions: {
            create: function(agent) {
                return {
                    cost: PARAPPLE.instructionCost.unit(agent),
                    result: agent.createObject("flower").key
                }
            }
        },
        constants: {
        }
    }

})