(function (ladybugs) {

    var self = this;

    this.placeRandomly = function(agent) {
        var tile = ladybugs.pixi2d.utils.nextRandomTile();
        agent.moveTo(tile.col, tile.row);
        agent.turn(ladybugs.pixi2d.gridAgents.orientations.TOP);
    }

    this.placeRandomlyAll = function(agentMap) {
        for (var agent of Object.values(agentMap)) {
            this.placeRandomly(agent);
        }
    }

    this.placeAllRandomlyInRange = function(c1, r1, c2, r2, agentMap) {
        var tiles = ladybugs.pixi2d.utils.shuffle(ladybugs.pixi2d.grid.getRange(c1, r1, c2, r2));
        for (var agent of Object.values(agentMap)) {
            var tile = tiles.pop();
            agent.moveTo(tile.col, tile.row);
            agent.turn(ladybugs.pixi2d.gridAgents.orientations.TOP);
        }
    }

    /*
     * Ladybug (a.k.a. Master)
     */
    this.typeLadybug = {
            name: "Ladybug",
            api: [
                {
                    name: "Ladybug",
                    doc: ladybugs.resources["apidocs/ladybug.html"]
                },
                ladybugs.pixi2d.apis.masterColoring,
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.walking,
                ladybugs.pixi2d.apis.flying,
                ladybugs.math.api,
                ladybugs.console.api,
                ladybugs.utils.api,
                ladybugs.utils.agents.api
            ]
        }

    this.makeLadybug = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "ladybug");
        agent.agentType = self.typeLadybug;
        agent.instructionCost = 1;
        return agent;
    }

    /*
     * Butterfly (a.k.a. Worker)
     */
    this.typeButterfly = {
            name: "Butterfly",
            api: [
                {
                    name: "Butterfly",
                    doc: ladybugs.resources["apidocs/butterfly.html"]
                },
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.flying,
                ladybugs.pixi2d.apis.coloring,
                ladybugs.pixi2d.apis.messages,
                ladybugs.math.api,
                ladybugs.console.api,
                ladybugs.utils.api,
                ladybugs.utils.agents.api
            ]
        }

    this.makeButterfly = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "butterfly");
        agent.agentType = self.typeButterfly;
        agent.instructionCost = 4; // 4x slower than Master Ladybug
        return agent;
    }

    /*
     * Beetle
     */
    this.typeBeetle = {
            name: "Beetle",
            api: [
                {
                    name: "Beetle",
                    doc: ladybugs.resources["apidocs/beetle.html"]
                },
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.walking,
                ladybugs.pixi2d.apis.coloring,
                ladybugs.math.api,
                ladybugs.console.api,
                ladybugs.utils.api,
                ladybugs.utils.agents.api
            ]
        }

    this.makeBeetle = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "beetle");
        agent.agentType = self.typeBeetle;
        return agent;
    }

    /*
     * Bee
     */
    this.typeBee = {
            name: "Bee",
            api: [
                {
                    name: "Bee",
                    doc: ladybugs.resources["apidocs/bee.html"]
                },
                ladybugs.forrest.api,
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.flying,
                ladybugs.pixi2d.apis.coloring,
                ladybugs.math.api,
                ladybugs.console.api,
                ladybugs.utils.api,
                ladybugs.utils.agents.api
            ]
        }

    this.makeBee = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "bee", true, 1); // blocking, capacity=1
        agent.agentType = self.typeBee;
        return agent;
    }

    /*
     * Snail
     */
    this.typeSnail = {
            name: "Snail",
            api: [
                {
                    name: "Snail",
                    doc: ladybugs.resources["apidocs/snail.html"]
                },
                ladybugs.console.api,
                ladybugs.utils.api,
                ladybugs.utils.agents.api,
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.messages,
                ladybugs.pixi2d.apis.walking
            ]
        }

    this.makeSnail = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "snail");
        agent.agentType = self.typeSnail;
        return agent;
    }

    /*
     * Lockstep Bug
     */
    this.typeBug = {
            name: "Bug",
            api: [
                {
                    name: "Bug",
                    doc: ladybugs.resources["apidocs/bug.html"]
                },
                ladybugs.broadcast.api,
                ladybugs.math.api,
                ladybugs.console.api,
                ladybugs.pixi2d.apis.coloring,
                ladybugs.pixi2d.apis.location,
                ladybugs.pixi2d.apis.walking,
                ladybugs.utils.api,
                ladybugs.utils.agents.api
            ]
        }

    this.makeBug = function(name) {
        var agent = ladybugs.pixi2d.gridAgents.makeAgent(name, "bug");
        agent.agentType = self.typeBug;
        return agent;
    }

})